package ;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.geom.Matrix;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;
import ui.CustomSlider;
import ui.CustomSprite;
import ui.UIConstructor;

/**
 * ...
 * @author tzverg
 */

class Doll_small extends CustomSprite
{
	public var uiConstructor:UIConstructor;
	public var body:CustomSprite;
	public var sliders:Array<CustomSlider>;
	public var img:Bitmap;

	public function new(X:Float=0, Y:Float=0) 
	{
		super(X, Y);
		
		uiConstructor = new UIConstructor(this, "doll_small_f.json", "screens/doll_small_f");
		uiConstructor.createUI();
		
		body = uiConstructor.uiElements.get("doll_small");
		
		var slidersNames:Array<String> = ["ink", "shadows", "eyes", "brows", "rouge", "pomade"];
		
		sliders = new Array();

		var itemSlider:CustomSlider;
		for (sliderName in slidersNames) {
			itemSlider = uiConstructor.uiElements.get(sliderName);
			sliders.push(itemSlider);
			if (Reg.dollParams != null && Reg.dollParams.exists(sliderName)) {
				itemSlider.setElement(Reg.dollParams.get(sliderName));
				var tmpBitmap:BitmapData = itemSlider.getCurrentBitmap();
			}
		}
	}
}