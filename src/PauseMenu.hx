package ;

import flash.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import ui.CustomButton;
import ui.CustomSlider;
import ui.CustomSprite;
import ui.UIConstructor;
import openfl.ui.Mouse;

/**
 * ...
 * @author tzverg
 */
class PauseMenu extends Sprite
{
	private var _name:String;
	private var uiConstructor:UIConstructor;
	
	private var background:CustomSprite;
	private var resumeButton:CustomSprite;
	private var quitButton:CustomSprite;
	private var Y8Logo:CustomSprite;
	private var idNetLogo:CustomSprite;
	private var moreGames:CustomSprite;
	
	public function new() 
	{
		super();
		_name = "pauseMenu_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();
		
		background = new CustomSprite();
		background.graphics.beginFill(0x000000, 0.7);
		background.graphics.drawRect(0.0, 0.0, uiConstructor.stageWidth, uiConstructor.stageHeight);
		background.graphics.endFill();
		
		this.addChildAt(background, 0);
		
		resumeButton = uiConstructor.uiElements.get("ResumeButton");
		resumeButton.addEventListener(TouchEvent.TOUCH_END, onResume);
		resumeButton.addEventListener(MouseEvent.CLICK, onResume);
		quitButton = uiConstructor.uiElements.get("QuitButton");
		quitButton.addEventListener(TouchEvent.TOUCH_END, onQuit);
		quitButton.addEventListener(MouseEvent.CLICK, onQuit);
		
		Y8Logo = uiConstructor.uiElements.get("Y8_Logo");
		Y8Logo.addEventListener(TouchEvent.TOUCH_END, onY8Logo);
		Y8Logo.addEventListener(MouseEvent.CLICK, onY8Logo);
		idNetLogo = uiConstructor.uiElements.get("idnet_logo");
		idNetLogo.addEventListener(TouchEvent.TOUCH_END, onIDNetLogo);
		idNetLogo.addEventListener(MouseEvent.CLICK, onIDNetLogo);
		
		moreGames = uiConstructor.uiElements.get("MoreGames");
		moreGames.addEventListener(TouchEvent.TOUCH_END, onMoreGames);
		moreGames.addEventListener(MouseEvent.CLICK, onMoreGames);
		
		#if flash
			if (!Links.isInnerDomain()) 
			{
				Y8Logo.buttonMode = true;
			}
			resumeButton.buttonMode = true;
			quitButton.buttonMode 	= true;
			idNetLogo.buttonMode 	= true;
			moreGames.buttonMode 	= true;
		#end
		
		if(Links.isInnerDomain()) {
			moreGames.visible = false;
		}
	}
	
	private function onMoreGames(e:MouseEvent):Void 
	{
		Links.gotoLink("moregames");
	}
		
	private function onIDNetLogo(e:MouseEvent):Void 
	{
		Links.openURL("http://id.net");
	}
	
	private function onY8Logo(e:MouseEvent):Void 
	{
		Links.gotoLink("mainmenu_logo");
	}
	
	private function onQuit(e:MouseEvent):Void 
	{
		Reg.soundManager.playSound("btnClick");
		
		Main.stateManager.onQuit();
	}
	
	private function onResume(e:MouseEvent):Void 
	{
		Reg.soundManager.playSound("btnClick");
		
		Main.stateManager.hidePauseMenu();
	}
}