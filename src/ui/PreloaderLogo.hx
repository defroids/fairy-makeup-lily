package ui;
import haxe.remoting.FlashJsConnection;
import idnet.common.events.IDNetEvent;
import idnet.Social;
import motion.Actuate;
import motion.actuators.GenericActuator;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Loader;
import openfl.display.MovieClip;
import openfl.display.Sprite;
import openfl.events.AsyncErrorEvent;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.events.NetStatusEvent;
import openfl.events.TouchEvent;
import openfl.media.Sound;
import openfl.net.NetConnection;
import openfl.net.NetStream;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.ui.Mouse;
import openfl.utils.ByteArray;
import openfl.media.Video;

#if html5
import js.Browser;
import js.html.CanvasElement;
import js.html.Document;
import js.html.ScriptElement;
#end


/**
 * ...
 * @author ...
 */
#if flash
@:file("../assets/preloader/flash/fml_preloader.swf") class PreloaderSWF extends ByteArray { }
#end
 
class PreloaderLogo extends Sprite
{
	private var _state:TzPreloader;
	private var bg:Sprite;
	private var preloader_logo_sprite:Sprite;
	private var preloader_logo_swf:MovieClip;
	private var textBg:Sprite;
	
	private var alert:Sprite;
	public var blacklisted:Bool;
	public var loaded:Bool;
	
	private var playButton:Sprite;
	private var present:TextField;
	private var gameName:TextField;
	private var frames:Array<Bitmap>;
	private var currentFrame:Int;
	private var maxFrames:Int;
	private var frameRate:Float;
	
	public  var finished:Bool;
	
	private var _video:Video;
	private var _stream:NetStream;
	private var _nc:NetConnection;
	public var firstFrame:MovieClip;
	public var loadingClip:MovieClip;
	
	#if html5
	public var _document:Document;
	#end
	
	public function new(state:TzPreloader) 
	{
		super();
		
		_state = state;
		
		bg = new Sprite();
		
		//bg.graphics.beginFill(0xF783BD);
		bg.graphics.beginFill(0x000000);
		bg.graphics.drawRect(0,0,800,533);
		bg.graphics.endFill();
		bg.x = 0;
		bg.y = 0;
		addChild(bg);
		finished = false;
		
		#if flash
		loaded = false;
		var loader:Loader = new Loader();
		loader.contentLoaderInfo.addEventListener(Event.COMPLETE, swfAssetLoaded);
		var bytes = new PreloaderSWF();
		loader.loadBytes(bytes);
		#end
		
		
		#if html5
		init();
		complete();
		#end
	}
	
	public function showAlert():Void
	{
		//trace("show alert");
		alert.visible = true;
	}
	
	function swfAssetLoaded(e:Event):Void
	{
		trace("swfAssetLoaded");
		
		/*if (!Links.isInnerDomain()) 
		{
			blacklisted = true;
		}*/
		
		preloader_logo_swf = cast(e.target.content, MovieClip);
		
		//preloader_logo_swf = Assets.getMovieClip("preloader_logo_swf:");
		
		if (preloader_logo_swf != null) {
			firstFrame = cast(preloader_logo_swf.getChildAt(1), MovieClip);
			loadingClip = cast(firstFrame.getChildByName("a"), MovieClip);
			loadingClip.stop();
			currentFrame = 0;
			maxFrames = firstFrame.totalFrames;
			frameRate = 24;
			//firstFrame.addFrameScript(firstFrame.totalFrames - 1, complete);
			/*var baseWidth:Float = preloader_logo_swf.width;
			preloader_logo_swf.scaleX = 800 / baseWidth;
			preloader_logo_swf.scaleY = 800 / baseWidth;
			preloader_logo_swf.y = 42;*/
			addChild(preloader_logo_swf);
			nextFrame();
		}
		
		init();
		loaded = true;
		Actuate.timer (2).onComplete (animationComplete);
	}
	
	function init():Void
	{
		try 
		{
			Social.i.init(Reg.app_id, Reg.salt, true, false, true);
		}  
		catch ( unknown : Dynamic )
		{
			trace("PreloaderLogo.Init(): unknown exclusion Social.i.init(): "+Std.string(unknown));
		}
		
		if (!Links.isInnerDomain())
		{
			preloader_logo_swf.getChildByName("btSponsPrel").addEventListener(MouseEvent.CLICK, onclickY8);
			preloader_logo_swf.getChildByName("btSponsPrel").addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
			preloader_logo_swf.getChildByName("btSponsPrel").addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
		}
		preloader_logo_swf.getChildByName("btIdNetPrel").addEventListener(MouseEvent.MOUSE_OVER, mouseOver);
		preloader_logo_swf.getChildByName("btIdNetPrel").addEventListener(MouseEvent.MOUSE_OUT, mouseOut);
		preloader_logo_swf.getChildByName("btIdNetPrel").addEventListener(MouseEvent.CLICK, onclickIdnet);
		
		alert = new CustomSprite(150.0, 150.0);
		alert.graphics.beginFill(0xFF0000, 0.7);
		alert.graphics.drawEllipse(0.0, 0.0, 500.0, 200.0);
		alert.graphics.endFill();
		alert.visible = false;
		
		var alertTextFormatIntro = new TextFormat("Arial", 16, 0xEFEFEF, true);
		alertTextFormatIntro.align = TextFormatAlign.CENTER;
		var alertTextFormatLink = new TextFormat("Arial", 18, 0xFFFF66);
		alertTextFormatLink.align = TextFormatAlign.CENTER;
		var alertTextFormatCompl = new TextFormat("Arial", 14, 0xEFEFEF);
		alertTextFormatCompl.align = TextFormatAlign.CENTER;
		
		var alertTextIntro = new TextField();
		alertTextIntro.defaultTextFormat = alertTextFormatIntro;
		alertTextIntro.text = "This site is blacklisted\nplease contact Y8.com team\nor play the game using this link";
		alertTextIntro.width = alertTextIntro.textWidth + 10.0;
		alertTextIntro.height = (alertTextIntro.textHeight + 10.0) * 3;
		alertTextIntro.x = Math.round(alert.width * 0.5 - alertTextIntro.width * 0.5);
		alertTextIntro.y = Math.round(alert.height * 0.05);
		alertTextIntro.selectable = false;
		
		var alertTextLink = new TextField();
		alertTextLink.defaultTextFormat = alertTextFormatLink;
		alertTextLink.text = "http://www.y8.com/games/" + Reg.slug;
		alertTextLink.width = alertTextLink.textWidth + 10.0;
		alertTextLink.height = (alertTextLink.textHeight + 10.0);
		alertTextLink.x = Math.round(alert.width * 0.5 - alertTextLink.width * 0.5);
		alertTextLink.y = Math.round(alert.height * 0.45);
		
		var alertTextCompletion = new TextField();
		alertTextCompletion.defaultTextFormat = alertTextFormatCompl;
		alertTextCompletion.text = "If you are website owner, please unblock\ngames link and request y8.com\nto remove you website from blacklist";
		alertTextCompletion.width = alertTextCompletion.textWidth + 10.0;
		alertTextCompletion.height = (alertTextCompletion.textHeight + 10.0) * 3;
		alertTextCompletion.x = Math.round(alert.width * 0.5 - alertTextCompletion.width * 0.5);
		alertTextCompletion.y = Math.round(alert.height * 0.65);
		alertTextCompletion.selectable = false;
		
		alert.addChild(alertTextIntro);
		alert.addChild(alertTextLink);
		alert.addChild(alertTextCompletion);
		addChild(alert);
		
		Social.i.addEventListener(IDNetEvent.ID_BLACKLISTED, function(e:Event):Void 
		{
			trace("ID_BLACKLISTED");
			alertTextLink.text = "http://www.y8.com/games/" + Reg.slug;
			blacklisted = true;
			showAlert();
			return;
		});
		
		playButton = new Sprite();
		playButton.x = 280;
		playButton.y = 320;
		
		textBg = new Sprite();
		textBg.graphics.beginFill(0xFF80D2);
		textBg.graphics.drawRect(0,0,250,50);
		textBg.graphics.endFill();
		textBg.x = 275;
		textBg.y = 255;
		
		present = new TextField();
		#if html5
		var presentFormat:TextFormat = new TextFormat("Arial", 28, 0x5e1d31, true);
		#else
		var presentFormat:TextFormat = new TextFormat("Arial Black", 24, 0x7a263f);
		#end
		present.defaultTextFormat = presentFormat;
		present.text = "Presents";
		present.selectable = false;
		present.width = 500;
		present.x = 335;
		present.y = 195;
		
		gameName = new TextField();
		#if html5
		var gameNameFormat:TextFormat = new TextFormat("Arial", 36, 0xd92263, true);
		#else
		var gameNameFormat:TextFormat = new TextFormat("Arial Black", 32, 0xd92263);
		#end
		gameName.defaultTextFormat = gameNameFormat;
		gameName.text = Reg.gameName;
		gameName.selectable = false;
		gameName.width = 500;
		gameName.x = 200;
		gameName.y = 255;
		
		#if html5
		present.x = 340;
		present.y = 175;
		gameName.x = 215;
		gameName.y = 245;
		playButton.y = 310;
		#end
		
		trace("blacklisted = " + blacklisted);
	}
	
	public function nextFrame():Void
	{
		if (Reg.gameLoaded == true)
		{
			var percentLoaded = Math.floor(_state.percentLoaded * 100);
			
			for (cnt in 0...percentLoaded)
			{
				loadingClip.nextFrame();
			}
		}
		
		if (loadingClip != null)
		{
			loadingClip.nextFrame();
		}
		
		if (currentFrame >= (maxFrames - 1)) {
			currentFrame = (maxFrames - 1);
		}
		
		/*if (!finished)
		{
			Actuate.timer (1.0 / frameRate).onComplete (nextFrame);
		}*/
	}
	
	static public function show(state:TzPreloader):PreloaderLogo
	{
		//trace("show");
		var preloader:PreloaderLogo = new PreloaderLogo(state);
		preloader.x = 0;
		preloader.y = 0;
		state.addChild(preloader);
		
		return preloader;
	}
	
	public function complete():Void
	{
		trace("complete load");
		
		if (preloader_logo_swf != null)
		{
			preloader_logo_swf.stop();
		}
		
		#if html5
		_state.preloaderFinish();
		#end
		#if flash
		//firstFrame.visible = false;
		//var fistElemet:MovieClip = cast(preloader_logo_swf.getChildAt(0), MovieClip);
		//fistElemet.play();
		//Actuate.timer (3).onComplete (animationComplete);
		animationComplete();
		#end
	}
	
	public function animationComplete():Void
	{		
		if (loaded && Reg.gameLoaded)
		{
			trace("animationComplete");
			finished = true;
			_state.onLoaded();
		}
	}
	
	private function mouseOver(e:MouseEvent):Void 
	{
		#if flash
			Mouse.cursor = "button";
		#end
	}
	
	private function mouseOut(e:MouseEvent):Void 
	{
		#if flash
			Mouse.cursor = "auto";
		#end
	}
	
	private function onclickY8(e:MouseEvent):Void 
	{
		if (!Links.isInnerDomain())
		{
			Links.gotoLink("mainmenu_logo");
		}
	}
	
	private function onclickIdnet(e:MouseEvent):Void 
	{
		Links.openURL("http://id.net");
	}
}