package ;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import openfl.ui.Mouse;
import ui.CustomButton;
import ui.CustomSlider;
import ui.CustomSprite;
import ui.MenuButton;
import ui.NextButton;
import ui.SoundButton;
import ui.UIConstructor;

/**
 * ...
 * @author tzverg
 */

class Screen2 extends Sprite
{
	private var _name:String;
	private var uiConstructor:UIConstructor;
	
	private var doll_small:Doll_small;
	private var nextButton:NextButton;
	public var sliderButtons:Array<CustomSprite>;
	public var sliders:Array<CustomSlider>;
	
	private var menuBtn:MenuButton;
	private var soundBtn:SoundButton;
	
	public function new() 
	{
		super();
		_name = "screen2_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();

		doll_small = uiConstructor.uiElements.get("doll_small");
		
		var slidersNames:Array<String> = ["ink", "shadows", "eyes", "brows", "rouge", "pomade"];
		Reg.dollParams = new Map();

		sliderButtons = new Array();
		sliders = new Array();

		var itemButton:CustomSprite;
		var itemSlider:CustomSlider;
		
		for (sliderName in slidersNames) 
		{
			itemButton = uiConstructor.uiElements.get(sliderName);
			
			#if flash
				itemButton.buttonMode = true;
			#end
			
			itemSlider = doll_small.uiConstructor.uiElements.get(sliderName);
			sliderButtons.push(itemButton);
			sliders.push(itemSlider);
			
			itemButton.addEventListener(TouchEvent.TOUCH_END, sliderButtonClick);
			itemButton.addEventListener(MouseEvent.CLICK, sliderButtonClick);
			Reg.dollParams.set(sliderName, 0);
		}
//		trace("Reg.dollParams = " + Reg.dollParams.toString());
	}

	private function sliderButtonClick(e:MouseEvent):Void
	{
		if (e.currentTarget != null) 
		{
			for (i in 0...sliderButtons.length) 
			{
				if (sliderButtons[i]._name == e.currentTarget.name) 
				{
					sliders[i].next();
//					trace("Reg.dollParams = " + Reg.dollParams.toString());
					Reg.dollParams.set(sliderButtons[i]._name, sliders[i]._currentID);
//					trace("Reg.dollParams = " + Reg.dollParams.toString());
					return;
				}
			}
		}
	}
	
}