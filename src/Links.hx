package ;
import haxe.ds.StringMap;
import openfl.net.URLRequest;
#if html5
import js.Browser;
#end
import openfl.Lib;

/**
 * ...
 * @author Codir
 */

class Links
{
	public function new() 
	{
		
	}
	
	static public function getInnerDomains():Array<String>
	{
		var innerDomains:Array<String> = new Array();
		innerDomains.push("y8.com");
		innerDomains.push("pog.com");
		innerDomains.push("gamepost.com");
		innerDomains.push("id.net");
		innerDomains.push("dollmania.com");
		innerDomains.push("games.xnxx.com");
		innerDomains.push("defroids.com");
		
		return innerDomains;
	}
	
	static public function getDomain():String
	{
		#if flash
		var url:String = Lib.current.stage.loaderInfo.url;
		#end
		#if html5
		var url:String = js.Browser.window.document.URL;
		#end
		url = url.toLowerCase();

		var real_domain:String = url;
		
		var domain_parts:Array<String> = url.split("://");
		if (domain_parts.length > 1) {
			real_domain = domain_parts[1];
			domain_parts = domain_parts[1].split("/");
			if (domain_parts.length > 1) {
				real_domain = domain_parts[0];
				domain_parts = domain_parts[0].split(".");
				if(domain_parts.length > 2) {
					real_domain = domain_parts[domain_parts.length - 2]+"."+domain_parts[domain_parts.length - 1];
				}
			}
		}

		return real_domain;
	}
	
	static public function isInnerDomain():Bool
	{
		var domain:String = Links.getDomain();
		var innerDomains:Array<String> = Links.getInnerDomains();
		
		trace(innerDomains.indexOf(domain) != -1);
		
		if (innerDomains.indexOf(domain) != -1) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function gotoLink(linkName:String):Void
	{
		var domain:String = Links.getDomain();
		
		var innerLinks:StringMap<String> = new StringMap();
		var outerLinks:StringMap<String> = new StringMap();
		
		innerLinks.set("preloader_logo", "http://www.y8.com/?utm_source="+domain+"&utm_medium=g_prelogo&utm_campaign="+Reg.gameName);
		outerLinks.set("preloader_logo", "http://www.y8.com/?utm_source="+domain+"&utm_medium=g_prelogo&utm_campaign="+Reg.gameName);
		
		innerLinks.set("mainmenu_logo", "http://www.y8.com/?utm_source="+domain+"&utm_medium=g_menulogo&utm_campaign="+Reg.gameName);
		outerLinks.set("mainmenu_logo", "http://www.y8.com/?utm_source="+domain+"&utm_medium=g_menulogo&utm_campaign="+Reg.gameName);
		
		innerLinks.set("moregames", "http://www.y8.com/?utm_source="+domain+"&utm_medium=g_moregames&utm_campaign="+Reg.gameName);
		outerLinks.set("moregames", "http://www.y8.com/?utm_source=" + domain + "&utm_medium=g_moregames&utm_campaign=" + Reg.gameName);
		
		var url:String = "";
		if (Links.isInnerDomain() && innerLinks.exists(linkName)) {
			url = innerLinks.get(linkName);
		} else if (outerLinks.exists(linkName)) {
			url = outerLinks.get(linkName);
		}
		
		if (!Links.isInnerDomain() && url != "") 
		{
			if (Reg.soundManager != null)
			{
				Reg.soundManager.playSound("btnClick");
			}
			
			#if flash
			Lib.getURL(new URLRequest (url));
			#end
			#if html5
			Lib.getURL(new URLRequest(url), "_blank");
			#end
		}
	}
	
	static public function openURL(url:String):Void
	{
		#if flash
		Lib.getURL(new URLRequest (url));
		#end
		#if html5
		Lib.getURL(new URLRequest(url), "_blank");
		#end
	}
}