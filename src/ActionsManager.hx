package ;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import ui.CustomSprite;
import motion.Actuate;

/**
 * ...
 * @author tzverg
 */

class ActionsManager
{
	public var count:Int;
	public var arrow:CustomSprite;
	
	private var _buttons:Array<CustomSprite>;
	private var _actions:Array<Int>;
	private var _cursors:Array<String>;
	private var _offsetsX:Array<Int>;
	private var _offsetsY:Array<Int>;
	private var _currentAction:Int;
	private var _currentActionId:Int;
	private var _screen:Screen1;
	
	public function new(screen:Screen1) 
	{
		_screen = screen;
		_buttons = new Array();
		_actions = new Array();
		_cursors = new Array();
		_offsetsX = new Array();
		_offsetsY = new Array();
		_currentAction = -1;
		_currentActionId = -1;
		
		arrow = _screen.arrow;
		count = 0;
	}
	
		
	public function addButton(button:CustomSprite, cursor:String):Void
	{
		_buttons.push(button);
		_cursors.push(cursor);
		_offsetsX.push(0);
		_offsetsY.push(0);
	}
	
	public function setCursor(i:Int, value:String, offsetX:Int = 0, offsetY:Int = 0):Void
	{
		_cursors[i] = value;
		_offsetsX[i] = offsetX;
		_offsetsY[i] = offsetY;
	}
	
	public function addAction(action:Int):Void
	{
		_actions.push(action);
	}
	
	public function nextAction():CustomSprite
	{
		if (_actions.length > 0) {
			
			/*if (_currentAction != -1 && _buttons[_currentAction] != null) {
				_buttons[_currentAction].removeEventListener(TouchEvent.TOUCH_END, onButton);
				_buttons[_currentAction].removeEventListener(MouseEvent.CLICK, onButton);
				ouchEvent.TOUCH
			}*/
			
			_currentActionId += 1;
			
			if (_actions.length > _currentActionId) 
			{
				if (_buttons.length > _actions[_currentActionId]) 
				{
					_currentAction = _actions[_currentActionId];
					
					arrow.visible = true;
					
					if (_currentAction > 3) 
					{
						arrow.scaleX = -1;
						#if html5
						arrow.x = Math.round(_buttons[_currentAction].x + arrow.width);
						#end
						#if flash
						arrow.x = Math.round(_buttons[_currentAction].x - arrow.width);
						#end
						arrow.y = Math.round(_buttons[_currentAction].y + _buttons[_currentAction].height * 0.5 - arrow.height * 0.5);
						arrowInRight();
					} 
					else 
					{
						arrow.scaleX = 1;
						arrow.x = Math.round(_buttons[_currentAction].x + _buttons[_currentAction].width + arrow.width);
						arrow.y = Math.round(_buttons[_currentAction].y + _buttons[_currentAction].height * 0.5 - arrow.height * 0.5);
						arrowInLeft();
					}
					
					trace("addEventListener " + _currentAction);
					_buttons[_currentAction].addEventListener(TouchEvent.TOUCH_END, onButton);
					_buttons[_currentAction].addEventListener(MouseEvent.CLICK, onButton);
					
					return _buttons[_currentAction];
				}
			} 
			else 
			{
				finish();
			}
		}
		
		_screen.arrow.visible = false;
		return null;
	}
	
	private function onButton(e:MouseEvent):Void 
	{
		_buttons[_currentAction].removeEventListener(TouchEvent.TOUCH_END, onButton);
		_buttons[_currentAction].removeEventListener(MouseEvent.CLICK, onButton);
		trace("SetCursor: "+_currentAction);
		_screen.setCursor(_cursors[_currentAction], _offsetsX[_currentAction], _offsetsY[_currentAction]);
		
		trace("onButton "+_currentAction);
		switch(_currentAction) 
		{
			case 0:
				_screen.showClock(); _screen.setCursorNone();
			case 4:
				count = _screen.brows.length;
				for (i in 0..._screen.brows.length) 
				{
					_screen.brows[i].addEventListener(TouchEvent.TOUCH_END, onCount);
					_screen.brows[i].addEventListener(MouseEvent.CLICK, onCount);
				}
				_screen.zoomIn();
			case 7:
				count = _screen.pimples.length;
				for (i in 0..._screen.pimples.length) 
				{
					_screen.pimples[i].addEventListener(TouchEvent.TOUCH_END, onCount);
					_screen.pimples[i].addEventListener(MouseEvent.CLICK, onCount);
				}
				_screen.zoomIn();
			default: _screen.cursor.addEventListener(TouchEvent.TOUCH_END, onCursor); _screen.cursor.addEventListener(MouseEvent.CLICK, onCursor);// _screen.doll_first.addEventListener(MouseEvent.CLICK, onDoll);
		}
	}
	
	private function onCursor(e:MouseEvent):Void 
	{
		trace("onCursor");
		if (_screen.cursor != null && _screen.cursor.hitTestObject(_screen.doll_first)) {
			_screen.cursor.removeEventListener(TouchEvent.TOUCH_END, onCursor);
			_screen.cursor.removeEventListener(MouseEvent.CLICK, onCursor);
			onDoll(e);
		}
	}
	
	private function onCount(e:MouseEvent):Void 
	{
		trace("onCount" + e);
		count -= 1;
		if (e.target != null) {
			e.target.removeEventListener(TouchEvent.TOUCH_END, onCount);
			e.target.removeEventListener(MouseEvent.CLICK, onCount);
			e.target.visible = false;
			#if html5
			_screen.doll_first.removeChild(e.target);
			#end
		}
		
		if (count <= 0) {
			_screen.unsetCursor();
			nextAction();
			_screen.zoomOut();
		}
	}
	
	private function onDoll(e:MouseEvent):Void 
	{
		trace("onDoll "+_currentAction);
		switch(_currentAction) {
			case 1: _screen.addChild(_screen.mask1); _screen.mask1.play(); nextAction();
			case 2: _screen.addChild(_screen.mask2); _screen.mask2.play(); nextAction();
			case 3: _screen.towelAnimation();
			case 5: _screen.addChild(_screen.mask3); _screen.mask3.play(); nextAction();
			case 6: _screen.showerAnimation();
			default: nextAction();
		}
		
		//_screen.doll_first.removeEventListener(MouseEvent.CLICK, onDoll);
		_screen.unsetCursor();
	}
	
	public function endAction():Void
	{
		trace("endAction "+_currentActionId);
		switch(_currentActionId) {
			case 1: _screen.removeChild(_screen.mask0); _screen.removeChild(_screen.mask1); nextAction();
			case 3: _screen.removeChild(_screen.mask2); nextAction();
			case 5: nextAction();
			case 6: nextAction();
			case 8: nextAction();
			case 9: _screen.removeChild(_screen.mask3);nextAction();
		}
	}

	
	private function finish():Void
	{
		_screen.arrow.visible = false;
		_screen.nextButton.visible = true;
	}
	
	public function arrowInLeft():Void
	{
		Actuate.tween(arrow, 0.5, { x: Math.round(arrow.x - arrow.width * 0.3)} ).onComplete(arrowOutLeft);
	}
	
	public function arrowOutLeft():Void
	{
		Actuate.tween(arrow, 2.0, { x: Math.round(arrow.x + arrow.width * 0.3)} ).onComplete(arrowInLeft);
	}
	
	public function arrowInRight():Void
	{
		Actuate.tween(arrow, 0.5, { x: Math.round(arrow.x + arrow.width * 0.3)} ).onComplete(arrowOutRight);
	}
	
	public function arrowOutRight():Void
	{
		Actuate.tween(arrow, 2.0, { x: Math.round(arrow.x - arrow.width * 0.3) } ).onComplete(arrowInRight);
	}
}