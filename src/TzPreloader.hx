package ;

import idnet.common.events.IDNetEvent;
import idnet.Social;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.display.Stage;
import openfl.events.Event;
import openfl.display.GradientType;
import openfl.events.MouseEvent;
import openfl.geom.Matrix;
import openfl.Lib;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import ui.CustomSprite;
import ui.PreloaderLogo;

/*@:bitmap("assets/preloader/html5/images/menuScreen_bg.png") class SimpleBitmapData extends BitmapData {}*/
#if js
	@:bitmap("assets/preloader/html5/fairy_preloader_bg.png") class Splash extends BitmapData { }
	@:bitmap("assets/preloader/html5/Y8_logo.png") class BunnerY8 extends BitmapData { }
	@:bitmap("assets/preloader/html5/id_net_logo.png") class BunnerIDNET extends BitmapData { }
#end


class TzPreloader extends NMEPreloader
{
	public var percentLoaded:Float;
	
	#if js
		public var blacklisted:Bool;
		
		private var alert:Sprite;
		private var percentText:TextField;
		private var bg:Bitmap;
		private var y8Logo:CustomSprite;
		private var idnetLogo:CustomSprite;
	#end
	
	#if flash
		private var spriteBg:Bitmap;
		private var stageWidth:Float;
		private var stageHeight:Float;
		private var progressBarStep:Float;
		private var preloaderLogo:PreloaderLogo;
	#end
	
	public function new() 
	{		
		super();
		#if js
			outline.visible = false;
		#end
		
		init();
	}
	
	private function init ():Void 
	{
		try 
		{
			Social.i.init(Reg.app_id, Reg.salt, true, false, true);
		}  
		catch ( unknown : Dynamic )
		{
			trace("PreloaderLogo.Init(): unknown exclusion Social.i.init(): "+Std.string(unknown));
		}
		
		#if flash
			//bg = new Sprite();
			outline = new Sprite();
			progress = new Sprite();
			
			stageWidth = 800;
			stageHeight = 533;
			
			progressBarStep = 0.01;
			
			/*//var backgroundColor = getBackgroundColor ();
			bg.graphics.beginFill(0xd0e487);
			bg.graphics.beginFill(0x00FF00);
			bg.graphics.drawRect(0, 0, stageWidth, stageHeight);
			bg.graphics.endFill();
			addChild (bg);*/
			
			/*var bgBitmapData = new SimpleBitmapData(Std.int(stageWidth), Std.int(stageHeight));
			spriteBg = new Bitmap(bgBitmapData, null, true);
			var bg = new Spr(bgBitmapData);
			addChild(spriteBg);
			trace("spriteBg added, spriteBg = " + spriteBg);*/
			
			outline.graphics.beginFill(0xCCCCCC);
			outline.graphics.drawRect(0, 0, 196, 32);
			outline.graphics.endFill();
			addChild (outline);
			
			progress.graphics.beginFill(0xd0e487);
			progress.graphics.drawRect(0, 0, 190, 26);
			progress.graphics.endFill();
			addChild (progress);
			
			outline.x = 302;
			outline.y = 254;
			
			progress.x = outline.x + 3;
			progress.y = outline.y + 3;
			
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		#end
		
		#if js
			bg = new Bitmap(new Splash(0,0));
			bg.smoothing = true;
			addChild(bg);
			
			var padding = 2;
			
			var barX = 30.0;
			var barY = getHeight() - 30.0;
			var barHeight = 26.0;
			var barWidth = getWidth () - barX * 2;
			
			outline = new Sprite ();
			outline.graphics.beginFill (0x25C2E0, 1);
			outline.graphics.drawRect (0, 0, barWidth, barHeight);
			outline.x = barX;
			outline.y = barY;
			outline.visible = false;
			addChild (outline);
			
			progress = new Sprite ();
			progress.graphics.beginFill (0xD164EC, 1);
			progress.graphics.drawRect (0, 0, barWidth - padding * 2, barHeight - padding * 2);
			progress.scaleX = 0;
			progress.x = barX + padding;
			progress.y = barY + padding;
			addChild (progress);
			
			//trace("progress: x:" + progress.x + ", y:" + progress.y + ", width:" + progress.width + ", height:" + progress.height + ", scale:" + progress.scaleX);
			
			var percentTextFormat = new TextFormat("Arial", 24, 0xFFD815, true);
			percentTextFormat.align = TextFormatAlign.CENTER;
			
			percentText = new TextField();
			percentText.defaultTextFormat = percentTextFormat;
			percentText.text = "100%";
			percentText.width = percentText.textWidth;
			percentText.height = percentText.textHeight;
			//percentText.border = true;
			percentText.x = Math.round(outline.x + barWidth * 0.5 - percentText.width * 0.5);
			percentText.y = Math.round(outline.y + barHeight * 0.5 - percentText.height * 0.5 + 4.0);
			percentText.selectable = false;
			percentText.visible = false;
			addChild (percentText);
			
			alert = new CustomSprite(150.0, 150.0);
			alert.graphics.beginFill(0xFF0000, 0.7);
			alert.graphics.drawEllipse(0.0, 0.0, 500.0, 200.0);
			alert.graphics.endFill();
			alert.visible = false;
			
			var alertTextFormatIntro = new TextFormat("Arial", 16, 0xEFEFEF, true);
			alertTextFormatIntro.align = TextFormatAlign.CENTER;
			var alertTextFormatLink = new TextFormat("Arial", 18, 0xFFFF66);
			alertTextFormatLink.align = TextFormatAlign.CENTER;
			var alertTextFormatCompl = new TextFormat("Arial", 14, 0xEFEFEF);
			alertTextFormatCompl.align = TextFormatAlign.CENTER;
			
			var alertTextIntro = new TextField();
			alertTextIntro.defaultTextFormat = alertTextFormatIntro;
			alertTextIntro.text = "This site is blacklisted\nplease contact Y8.com team\nor play the game using this link";
			alertTextIntro.width = alertTextIntro.textWidth + 10.0;
			alertTextIntro.height = (alertTextIntro.textHeight + 10.0) * 3;
			alertTextIntro.x = Math.round(alert.width * 0.5 - alertTextIntro.width * 0.5);
			alertTextIntro.y = Math.round(alert.height * 0.05);
			alertTextIntro.selectable = false;
			
			var alertTextLink = new TextField();
			alertTextLink.defaultTextFormat = alertTextFormatLink;
			alertTextLink.text = "http://www.y8.com/games/" + Reg.slug;
			alertTextLink.width = alertTextLink.textWidth + 10.0;
			alertTextLink.height = (alertTextLink.textHeight + 10.0);
			alertTextLink.x = Math.round(alert.width * 0.5 - alertTextLink.width * 0.5);
			alertTextLink.y = Math.round(alert.height * 0.45);
			
			var alertTextCompletion = new TextField();
			alertTextCompletion.defaultTextFormat = alertTextFormatCompl;
			alertTextCompletion.text = "If you are website owner, please unblock\ngames link and request y8.com\nto remove you website from blacklist";
			alertTextCompletion.width = alertTextCompletion.textWidth + 10.0;
			alertTextCompletion.height = (alertTextCompletion.textHeight + 10.0) * 3;
			alertTextCompletion.x = Math.round(alert.width * 0.5 - alertTextCompletion.width * 0.5);
			alertTextCompletion.y = Math.round(alert.height * 0.65);
			alertTextCompletion.selectable = false;
			
			alert.addChild(alertTextIntro);
			alert.addChild(alertTextLink);
			alert.addChild(alertTextCompletion);
			addChild(alert);
			
			/*if (!Links.isInnerDomain()) 
			{
				blacklisted = true;
			}*/
			
			Social.i.addEventListener(IDNetEvent.ID_BLACKLISTED, function(e:Event):Void 
			{
				trace("ID_BLACKLISTED");
				alertTextLink.text = "http://www.y8.com/games/" + Reg.slug;
				blacklisted = true;
				showAlert();
				return;
			});
			
			var y8LogoBitmap = new Bitmap(new BunnerY8(0, 0));
			y8Logo = new CustomSprite();
			y8Logo.addChild(y8LogoBitmap);
			y8Logo.x = 8.0;
			y8Logo.y = 8.0;
			addChild (y8Logo);
			
			var idnetLogoBitmap = new Bitmap(new BunnerIDNET(0, 0));
			idnetLogo = new CustomSprite();
			idnetLogo.addChild(idnetLogoBitmap);
			idnetLogo.x = 690.0;
			idnetLogo.y = 8.0;
			addChild (idnetLogo);
			
			y8Logo.addEventListener(MouseEvent.CLICK, onclickY8);
			idnetLogo.addEventListener(MouseEvent.CLICK, onclickIdnet);
			
			trace("blacklisted = " + blacklisted);
		#end
    }
	
	override public function onUpdate(bytesLoaded:Int, bytesTotal:Int)
	{
		percentLoaded = bytesLoaded / bytesTotal;
		//trace("onUpdate, percentLoaded = " + percentLoaded);
		//trace("onUpdate, percentLoaded = " + Math.floor(percentLoaded * 100));
		
		#if js
			percentText.text = Std.string(Math.floor(percentLoaded * 100)) + "%";
			progress.scaleX = percentLoaded;
			
			if (percentLoaded != 0 && (!percentText.visible || !outline.visible))
			{
				percentText.visible = true;
				outline.visible = true;
			}
		#end
		
		#if flash
			if (percentLoaded > progressBarStep)
			{
				preloaderLogo.nextFrame();
				progressBarStep += 0.01;
			}
			if (percentLoaded >= 1 && !preloaderLogo.blacklisted)
			{
				Reg.gameLoaded = true;
				preloaderLogo.complete();
				//dispatchEvent (new Event (Event.COMPLETE));
			}
		#end
	}
	
	#if js
		override public function onLoaded () 
		{
			if (!blacklisted)
			{
				super.onLoaded();
			}
			else
			{
				showAlert();
			}
		}
		public function showAlert():Void
		{
			alert.visible = true;
		}
		private function onclickY8(e:MouseEvent):Void 
		{
			if (!Links.isInnerDomain())
			{
				Links.gotoLink("mainmenu_logo");
			}
		}
		private function onclickIdnet(e:MouseEvent):Void 
		{
			Links.openURL("http://id.net");
		}	
		public function preloaderFinish():Void
		{
			if (progress.scaleX >= 1.0) 
			{
				dispatchEvent (new Event (Event.COMPLETE));
			}
		}
	#end
	
	#if flash
		private var stageLink:Stage;
		
		private function onAdded(e:Event):Void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			stageLink = this.stage;
			this.stage.addEventListener(Event.RESIZE, onResize);
			
			onResize(null);
			
			preloaderLogo = PreloaderLogo.show(this);
		}
		
		override public function onInit() 
		{
			super.onInit();
		}
		
		private function onResize(e:Event):Void
		{
			if (stageLink != null && stageLink.stageWidth > 0) stageWidth = stageLink.stageWidth;
			if (stageLink != null && stageLink.stageHeight > 0) stageHeight = stageLink.stageHeight;
			
			var stageRatio = stageWidth / stageHeight;
			var screenRatio = 800 * this.scaleX / 533 * this.scaleY;
			
			//bg.width = stageWidth;
			//bg.height = stageHeight;
			
			//spriteBg.scaleX = 
			
			var newScaleX:Float;
			var newScaleY:Float;
			if (screenRatio > stageRatio)
			{
				var newRatio = stageWidth / 800;
				newScaleX = newRatio > 1? newRatio : 1 ;
				newScaleY = newScaleX;
			}
			else 
			{
				var newRatio = stageHeight / 533;
				newScaleY = newRatio > 1? newRatio : 1 ;
				newScaleX = newScaleY;
			}
			
			var newX:Float = Std.int((stageWidth / 2 - outline.width / 2));
			var newY:Float = Std.int((stageHeight / 2 - outline.height / 2));
			outline.x = newX;
			outline.y = newY;
			
			progress.x = outline.x + 3;
			progress.y = outline.y + 3;
		}
		
		private function onRemoved(e:Event):Void
		{
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemoved);
			this.stageLink.removeEventListener(Event.RESIZE, onResize);
			stageLink = null;
		}
		
		override public function onLoaded()
		{
			if (preloaderLogo.finished) 
			{
				if (preloaderLogo.blacklisted)
				{
					preloaderLogo.showAlert();
					return;
				}
				else
				{
					dispatchEvent (new Event (Event.COMPLETE));
				}
			}
		}
	#end;
}