package ;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.BlendMode;
import openfl.display.Sprite;
import openfl.geom.Matrix;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import openfl.Lib;
import ui.CustomSlider;
import ui.CustomSprite;
import ui.UIConstructor;

/**
 * ...
 * @author Codir
 */
class Doll extends CustomSprite
{
	public var uiConstructor:UIConstructor;
	public var body:CustomSprite;
	public var sliders:Array<CustomSlider>;
	public var img:Bitmap;

	public function new(X:Float=0, Y:Float=0)
	{
		super(X, Y);
		
		uiConstructor = new UIConstructor(this, "doll_f.json", "screens/doll_f");
		uiConstructor.createUI();
		
		body = uiConstructor.uiElements.get("doll_f");
		
		var slidersNames:Array<String> = ["accessory", "hairs", "jewelry", "cloth_top", "cloth_bottom", "wings"];
		var slidersNames2:Array<String> = ["eyes", "pomade", "ink", "rouge", "brows", "shadows"];
		
		sliders = new Array();
		
		var itemSlider:CustomSlider;
		for (sliderName in slidersNames) 
		{
			itemSlider = uiConstructor.uiElements.get(sliderName);
			
			sliders.push(itemSlider);
			if (Reg.dollParams != null && Reg.dollParams.exists(sliderName)) 
			{
				itemSlider.setElement(Reg.dollParams.get(sliderName));
				var tmpBitmap:BitmapData = itemSlider.getCurrentBitmap();
			}
		}
		
		for (sliderName in slidersNames2) 
		{
			itemSlider = uiConstructor.uiElements.get(sliderName);
			sliders.push(itemSlider);
			
			itemSlider.scaleX = 0.33;
			itemSlider.scaleY = 0.33;
			if (Reg.dollParams != null && Reg.dollParams.exists(sliderName)) 
			{
				itemSlider.setElement(Reg.dollParams.get(sliderName));
				var tmpBitmap:BitmapData = itemSlider.getCurrentBitmap();
			}
		}
	}
	
	public function setDresses():Void
	{
		if (Reg.dresses) 
		{
			/*sliders[1].visible = true;
			sliders[3].visible = false;
			sliders[4].visible = false;*/
		} 
		else 
		{
			/*sliders[1].visible = false;
			sliders[3].visible = true;
			sliders[4].visible = true;*/
		}
	}
	
	public function getBitmap():BitmapData
	{
		var dollBitmap:BitmapData = new BitmapData(Math.floor(width), Math.floor(height), true, 0x000000FF);
		dollBitmap.draw(this);
		
		return dollBitmap;
	}
}