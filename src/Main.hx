package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import idnet.common.events.IDNetEvent;
import idnet.Social;

/**
 * ...
 * @author Codir
 */

class Main extends Sprite 
{
	public static var stateManager:StateManager;
	public static var soundManager:SoundManager;
	
	private var inited:Bool;

	/* ENTRY POINT */
	
	function resize(e) 
	{		
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		trace("Main: Init():");
		if (inited) return;
		inited = true;
		
		Social.i.addEventListener(IDNetEvent.ID_INITIALIZE_COMPLETE, onIDInitComplete);
		Social.i.addEventListener(IDNetEvent.ID_INITIALIZE_FAILED, onIDInitFailed);
		
		soundManager = new SoundManager();
		stateManager = new StateManager(this);
		
		Reg.soundManager = soundManager;
		Reg.stateManager = stateManager;
		
		var Y8SDK:Y8Data = new Y8Data();
		Y8SDK.init();
		
		stateManager.switchToNextState();
		
		//For test
		//onIDInitComplete(null);
		//
	}
	
	private function onIDInitFailed(e:Event):Void
	{
		trace("onIDInitFailed(): ID_INITIALIZE_FAILED");
		onIDInitComplete(null);
	}
	
	private function onIDInitComplete(e:Event):Void
	{		
		/*#if flash
		if (mainSound.bytesLoaded != 0) {
			mainSoundChannel = mainSound.play(musicPos, 1000);
		}
		#end
		#if !flash
		mainSoundChannel = mainSound.play(musicPos, 1000);
		#end*/
	}

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	private function odDelay():Void {
		resize(null);
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}
}
