package ;
import motion.Actuate;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.ui.Mouse;
import openfl.ui.Mouse;
import ui.CustomButton;
import ui.CustomSprite;
import ui.MenuButton;
import ui.NextButton;
import ui.SoundButton;
import ui.UIConstructor;

/**
 * ...
 * @author tzverg
 */

 import com.marshgames.openfltexturepacker.TexturePackerImport;
 import openfl.display.Tilesheet;
 
class Screen1 extends Sprite
{
	public var arrow:CustomSprite;
	public var baseArrowX:Float;
	public var baseArrowY:Float;
	public var doll_first:CustomSprite;
	public var doll_brows:CustomSprite;
	public var nextButton:NextButton;
	public var cursor:Sprite;
	public var pimples:Array<CustomSprite>;
	public var brows:Array<CustomSprite>;
	public var mask0:CustomSprite;
	public var mask1:AnimatedSprite;
	public var mask2:AnimatedSprite;
	public var mask3:AnimatedSprite;
	public var clock:Clock;
	public var shower:CustomSprite;
	public var showerBaseX:Float;
	public var showerBaseY:Float;
	public var clockBaseX:Float;
	public var clockBaseY:Float;
	public var cursorOffsetX:Int;
	public var cursorOffsetY:Int;
	
	private var _name:String;
	private var uiConstructor:UIConstructor;
	
	private var actionManager:ActionsManager;
	private var menuBtn:MenuButton;
	private var soundBtn:SoundButton;
	private var towelBaseX:Float;
	private var towelBaseY:Float;
	private var towelTargetArrayX:Array<Float>;
	private var towelTargetArrayY:Array<Float>;
	private var towelTargetCount:Int;
	private var towel:Sprite;
	private var doll_firstBaseX:Float;
	private var doll_firstBaseY:Float;
	private var fruits:Sprite;
	private var offsetOnEyeX:Float;
	private var offsetOnEyeY:Float;
	
	public function new() 
	{		
		super();
		_name = "screen1_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();
		
		arrow = uiConstructor.uiElements.get("arrow");
		if (arrow != null) {
			baseArrowX = arrow.x;
			baseArrowY = arrow.y;
		}
		arrow.visible = false;
		
		nextButton = uiConstructor.uiElements.get("nextBtn");
		
		doll_first = uiConstructor.uiElements.get("doll_first");
		doll_first.scaleX  = 0.65;
		doll_first.scaleY  = 0.65;
		
		doll_firstBaseX = doll_first.x;
		doll_firstBaseY = doll_first.y;
		
		mask0 = uiConstructor.uiElements.get("mask_0");
		
		clock = uiConstructor.uiElements.get("clock");
		clock._screen = this;
		clockBaseX = clock.x;
		clockBaseY = clock.y;
		clock.visible = false;
		
		shower = uiConstructor.uiElements.get("shower");
		showerBaseX = shower.x;
		showerBaseY = shower.y;
		shower.visible = false;
		
		fruits = uiConstructor.uiElements.get("fruits_on_eye");		
		this.removeChild(fruits);
		
		offsetOnEyeX = fruits.x;
		offsetOnEyeY = fruits.y;
		
		fruits = null;
		
		cursorOffsetX = 0;
		cursorOffsetY = 0;
		nextButton.visible = false;
		
		pimples = new Array();
		brows = new Array();
		
		actionManager = new ActionsManager(this);
		
		var action:CustomSprite;
		for (i in 1...9) 
		{
			action = uiConstructor.uiElements.get("action" + i);
			
			#if flash
				action.buttonMode = true;
			#end
			
			if (action != null) {
				actionManager.addButton(action, "cursor" + i + ".png");
			}
		}
		
		#if !html5
		actionManager.setCursor(1, "cursor1.png");
		actionManager.setCursor(2, "cursor2.png");
		actionManager.setCursor(3, "cursor3.png");
		actionManager.setCursor(4, "cursor4.png", 74, 52);//!
		actionManager.setCursor(5, "cursor5.png");
		actionManager.setCursor(6, "cursor6.png");
		actionManager.setCursor(7, "cursor7.png", 70, 54);//!
		#end
		
		#if html5
		actionManager.setCursor(1, "cursor1.png", -72, -55); 		// 143;110
		actionManager.setCursor(2, "cursor2.png", -72, -56); 		// 143;111
		actionManager.setCursor(3, "cursor3.png", -62, -63); 		// 124;125
		actionManager.setCursor(4, "cursor4.png", 75-74, 53-52);	//!148;103
		actionManager.setCursor(5, "cursor5.png", -72, -56); 		// 143;111
		actionManager.setCursor(6, "cursor6.png", -132, -103); 		// 263;206
		actionManager.setCursor(7, "cursor7.png", 71-70, 55-54); 	//!139;108
		#end
		
		towelTargetArrayX = new Array();
		towelTargetArrayY = new Array();
		
		var item:CustomSprite;
		for (i in 1...6) 
		{
			item = uiConstructor.uiElements.get("cos_fatza_" + i);
			if (item != null) 
			{
				towelTargetArrayX.push(item.x - 75.0);
				towelTargetArrayY.push(item.y);
			}
		}
		
		var item_cos:CustomSprite;
		for (i in 1...8) 
		{
			item_cos = uiConstructor.uiElements.get("cos_fatza_" + i);
			if (item_cos != null)
			{
				item_cos.x *= 1.54;
				item_cos.y *= 1.54;
				removeChild(item_cos);
				doll_first.addChild(item_cos);
				item_cos.x -= doll_first.x;
				item_cos.y -= doll_first.y;
				pimples.push(item_cos);
			}
		}
		
		var item_fir:CustomSprite;
		for (i in 1...11) 
		{
			item_fir = uiConstructor.uiElements.get("fir_" + i);
			if (item_fir != null) 
			{
				item_fir.x *= 1.54;
				item_fir.y *= 1.54;
				removeChild(item_fir);
				doll_first.addChild(item_fir);
				item_fir.x -= doll_first.x;
				item_fir.y -= doll_first.y;
				item_fir.x += 3.0;
				item_fir.y += 13.0;
				brows.push(item_fir);
			}
		}
		
		mask1 = new AnimatedSprite("masks/mask_f_2", 39, 16);
		mask2 = new AnimatedSprite("masks/mask_f_1", 39, 16);
		mask3 = new AnimatedSprite("masks/mask_f_3", 39, 16);
		
		//addChild(mask1);
		mask1.x = mask0.x;
		mask1.y = mask0.y;
	
		
		//addChild(mask2);
		mask2.x = mask0.x;
		mask2.y = mask0.y;
		
		//addChild(mask3);
		mask3.x = mask0.x;
		mask3.y = mask0.y;
		
		actionManager.addAction(1); //soap(mask1)
		actionManager.addAction(6); //shower
		actionManager.addAction(2); //mask2
		actionManager.addAction(6); //shower
		actionManager.addAction(7); //pimple
		actionManager.addAction(3); //towel
		actionManager.addAction(4); //tweezers
		actionManager.addAction(5); //mask3
		actionManager.addAction(0); //fruits(clock)
		actionManager.addAction(6); //shower
		
		actionManager.nextAction();
	}
	
	public function zoomIn():Void
	{
		var offsetW:Float = (doll_first.width - (doll_first.width * 0.54)) * 0.5;
		var offsetH:Float = (doll_first.height - (doll_first.height * 0.54)) * 0.5;
		
		Actuate.tween(doll_first, 0.5, { x: doll_firstBaseX - offsetW - 50.0, y: doll_firstBaseY - offsetH, scaleX: 1, scaleY:  1 } );
	}
	
	public function zoomOut():Void
	{
		Actuate.tween(doll_first, 0.5, { x: doll_firstBaseX, y: doll_firstBaseY, scaleX: 0.65, scaleY:  0.65 } );
	}
	
	public function addFruitsOnEye():Void
	{		
//		trace("addFruitsOnEye: fruits = " + fruits);
		if (fruits == null) 
		{
			fruits = new Sprite();
			uiConstructor.tilesheet.drawTiles(fruits.graphics, [0, 0, uiConstructor.idMap["eyemask.png"]]);
			addChild(fruits);
			
			#if flash
			fruits.x = uiConstructor.uiElements.get("action1").x;
			fruits.y = uiConstructor.uiElements.get("action1").y;
			#end
			
			#if html5
			fruits.x = 84;
			fruits.y = 54;
			#end
			
			Actuate.tween(fruits, 1.0, { x: offsetOnEyeX, y: offsetOnEyeY } );
		}
	}
	
	public function showerAnimation():Void
	{
		addChild(shower);
		shower.visible = true;
		shower.x = showerBaseX;
		shower.y = showerBaseY;
		
		Actuate.tween(shower, 0.5, {rotation: 10, x: showerBaseX - 20} ).onComplete(showerAnimation2);
	}
	
	function showerAnimation2():Void
	{
		Actuate.tween(shower, 1.0, {rotation: 0, x: showerBaseX - 5} ).onComplete(showerAnimation3);
	}
	
	function showerAnimation3():Void
	{
		Actuate.tween(shower, 1.0, {rotation: 20, y: showerBaseY - 50} ).onComplete(showerAnimation4);
	}
	
	function showerAnimation4():Void
	{
		removeChild(shower);
		shower.visible = false;
		actionManager.endAction();
	}
	
	public function towelAnimation():Void
	{
		towel = new Sprite();
		towelTargetCount = 0;
		
		for (i in 0...5)
		{
			trace("towelTargetArrayX[" + i + "] = " + towelTargetArrayX[i]);
			trace("towelTargetArrayY[" + i + "] = " + towelTargetArrayY[i]);
		}
		
		uiConstructor.tilesheet.drawTiles(towel.graphics, [0, 0, uiConstructor.idMap["cursor3.png"]]);
		addChild(towel);
		
		towelBaseX = towelTargetArrayX[towelTargetCount];
		towelBaseY = towelTargetArrayY[towelTargetCount];
		towelTargetCount++;
		
		towel.x = towelBaseX;
		towel.y = towelBaseY;
		Actuate.tween(towel, 0.5, { x: towelTargetArrayX[towelTargetCount], y: towelTargetArrayY[towelTargetCount++] } ).onComplete(towelAnimation2);
	}
	
	function towelAnimation2() 
	{
		Actuate.tween(towel, 0.5, { x: towelTargetArrayX[towelTargetCount], y: towelTargetArrayY[towelTargetCount++] } ).onComplete(towelAnimation3);
	}
	
	function towelAnimation3() 
	{
		Actuate.tween(towel, 0.5, { x: towelTargetArrayX[towelTargetCount], y: towelTargetArrayY[towelTargetCount++] } ).onComplete(towelAnimation4);
	}
	
	function towelAnimation4() 
	{
		Actuate.tween(towel, 0.5, { x: towelTargetArrayX[towelTargetCount], y: towelTargetArrayY[towelTargetCount++] } ).onComplete(towelAnimation5);
	}
	
	function towelAnimation5() 
	{
		if (towel != null) 
		{
			towel.visible = false;
			removeChild(towel);
			
			towelTargetCount = 0;
			
			towel = null;
			
			towelTargetArrayX = null;
			towelTargetArrayY = null;
		}
		actionManager.endAction();
	}
	
	public function showClock():Void
	{
		trace("showClock started");
		addFruitsOnEye();
		addChild(clock);
		clock.x = -clock.width;
		clock.visible = true;
		Actuate.tween(clock, 1.0, {x: clockBaseX, y: clockBaseY } ).onComplete(showClockComplete);
	}
	
	function showClockComplete():Void
	{
		clock.play();
	}
	
	public function hideClock():Void
	{
		Actuate.tween(clock, 1.0, { x: -clock.width } ).onComplete(hideClockComplete);
		Actuate.tween(fruits, 1.0, { x: stage.width} );
		actionManager.endAction();
		Mouse.show();
	}
	
	function hideClockComplete():Void
	{
		removeChild(clock);
		clock.visible = false;
	}
	
	public function setCursor(name:String, offsetX:Int = 0, offsetY:Int = 0):Void
	{
		cursor = new Sprite();
		uiConstructor.tilesheet.drawTiles(cursor.graphics, [0, 0, uiConstructor.idMap[name]]);
		addChild(cursor);
		trace(name, cursor.width, cursor.height);
		#if !html5
		cursor.x = mouseX - cursor.width/2 + offsetX;
		cursor.y = mouseY - cursor.height / 2 + offsetY;
		#end
		#if html5
		cursor.x = mouseX + offsetX;
		cursor.y = mouseY + offsetY;
		#end
		cursorOffsetX = offsetX;
		cursorOffsetY = offsetY;
		addEventListener(MouseEvent.MOUSE_MOVE, onMove);
		Mouse.hide();
	}
	
	public function setCursorNone():Void
	{
		if (cursor != null) {
			cursor.visible = false;
		}
		Mouse.hide();
	}

	public function unsetCursor():Void
	{
		removeChild(cursor);
		removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
		Mouse.show();
	}
	
	private function onMove(e:MouseEvent):Void 
	{
		if (cursor != null && cursor.visible) {
			#if !html5
			cursor.x = mouseX - cursor.width/2 + cursorOffsetX;
			cursor.y = mouseY - cursor.height / 2 + cursorOffsetY;
			#end
			#if html5
			cursor.x = mouseX + cursorOffsetX;
			cursor.y = mouseY + cursorOffsetY;
			#end
		}
	}
	
	private function clockTest(e:MouseEvent):Void 
	{
		clock.play();
	}
}