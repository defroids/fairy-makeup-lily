package ;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import openfl.ui.Mouse;
import ui.CustomButton;
import ui.CustomSlider;
import ui.CustomSprite;
import ui.MenuButton;
import ui.NextButton;
import ui.SoundButton;
import ui.UIConstructor;

/**
 * ...
 * @author tzverg
 */

class Screen3 extends Sprite
{
	private var _name:String;
	private var uiConstructor:UIConstructor;
	
	private var doll:Doll;
	
	private var nextButton:NextButton;
	private var menuBtn:MenuButton;
	private var soundBtn:SoundButton;
	
	public var sliderButtons:Array<CustomSprite>;
	public var sliders:Array<CustomSlider>;
	
	public function new() 
	{
		super();
		_name = "screen3_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();

		doll = uiConstructor.uiElements.get("doll");
		
		var slidersNames:Array<String> = ["accessory", "hairs", "jewelry", "cloth_top", "cloth_bottom", "wings"];

		sliderButtons = new Array();
		sliders = new Array();

		var itemButton:CustomSprite;
		var itemSlider:CustomSlider;
		
		for (sliderName in slidersNames) 
		{
			itemButton = uiConstructor.uiElements.get(sliderName);
			itemSlider = doll.uiConstructor.uiElements.get(sliderName);
			
			#if flash
				itemButton.buttonMode = true;
			#end
			
			sliderButtons.push(itemButton);
			sliders.push(itemSlider);
			
			itemButton.addEventListener(TouchEvent.TOUCH_END, sliderButtonClick);
			itemButton.addEventListener(MouseEvent.CLICK, sliderButtonClick);
			Reg.dollParams.set(sliderName, 0);
		}
		
		nextButton = uiConstructor.uiElements.get("nextBtn");
		soundBtn = uiConstructor.uiElements.get("soundBtn");
		menuBtn = uiConstructor.uiElements.get("menuBtn");
	}
	
	private function sliderButtonClick(e:MouseEvent):Void
	{
		if (e.currentTarget != null) 
		{
			for (i in 0...sliderButtons.length) 
			{
				if (sliderButtons[i]._name == e.currentTarget.name) 
				{
					sliders[i].next();
					Reg.dollParams.set(sliderButtons[i]._name, sliders[i]._currentID);
					return;
				}
			}
		}
	}
}