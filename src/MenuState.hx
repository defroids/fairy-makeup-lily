package ;
import idnet.common.events.IDNetEvent;
import idnet.Social;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.events.TouchEvent;
import openfl.ui.Mouse;
import openfl.text.TextField;
import openfl.text.TextFormat;
import ui.CustomSprite;
import ui.FinalPopup;
import ui.HelpingPopup;
import ui.NextButton;
import ui.SoundButton;
import ui.UIConstructor;

/**
 * ...
 * @author tzverg
 */

class MenuState extends Sprite
{
	private var _name:String;
	private var uiConstructor:UIConstructor;
	private var registerBtn:CustomSprite;
	private var Y8Logo:CustomSprite;
	private var idNetLogo:CustomSprite;
	private var status:TextField;
	private var playBtn:NextButton;
	private var soundBtn:SoundButton;
	private var moreGames:CustomSprite;
	
	private var popup:HelpingPopup;
	
	public function new() 
	{
		super();
		_name = "menu_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();
		
		moreGames = uiConstructor.uiElements.get("moreGames");
		registerBtn = uiConstructor.uiElements.get("registerBtn");
		Y8Logo = uiConstructor.uiElements.get("Y8_Logo");		
		idNetLogo = uiConstructor.uiElements.get("idnet_logo");
		playBtn = uiConstructor.uiElements.get("playBtn");
		soundBtn = uiConstructor.uiElements.get("soundBtn");
		
		#if flash
			if (!Links.isInnerDomain()) 
			{
				Y8Logo.buttonMode = true;
			}
			moreGames.buttonMode 	= true;
			registerBtn.buttonMode 	= true;
			idNetLogo.buttonMode 	= true;
		#end
		
		popup = new HelpingPopup("Please login to save your creation on your y8 profile\nto share with your friends", uiConstructor);
		this.addChild(popup);
		
		registerBtn.addEventListener(TouchEvent.TOUCH_END, _login);
		registerBtn.addEventListener(MouseEvent.CLICK, _login);
		registerBtn.addEventListener(MouseEvent.MOUSE_OVER,registerMouseOver);
		registerBtn.addEventListener(MouseEvent.MOUSE_OUT, registerMouseOut);
		
		Y8Logo.addEventListener(TouchEvent.TOUCH_END, onY8Logo);
		idNetLogo.addEventListener(TouchEvent.TOUCH_END, onIDNetLogo);
		moreGames.addEventListener(TouchEvent.TOUCH_END, moreGamesClick);
		
		Y8Logo.addEventListener(MouseEvent.CLICK, onY8Logo);
		idNetLogo.addEventListener(MouseEvent.CLICK, onIDNetLogo);
		moreGames.addEventListener(MouseEvent.CLICK, moreGamesClick);
		
		var myFormat:TextFormat = new TextFormat();
		myFormat.size = 22;
		
		status = new TextField();
		status.text = "";
		status.width = 300;
		status.height = 40;
		status.borderColor = 0x000000;
		status.selectable = false;
		status.textColor = 0x000000;
		status.alpha = 0.8;
		status.defaultTextFormat = myFormat;
		status.text = "";
		status.x = 5;
		status.y = 5;
		addChild(status);
		
		if (Links.isInnerDomain()) 
		{
			moreGames.visible = false;
		}
		
		init();
//		_logout(null);
	}
		
	private function moreGamesClick(e:MouseEvent):Void 
	{
		Links.gotoLink("moregames");
	}
	
	private function registerMouseOver(e:MouseEvent):Void 
	{
		popup.x = Math.round(registerBtn.x + registerBtn.width * 0.7);
		popup.y = Math.round(registerBtn.y + registerBtn.height * 0.5 - popup.height * 1.2);
		
		popup.visible = true;
	}
	
	private function registerMouseOut(e:MouseEvent):Void 
	{
		popup.visible = false;
	}
	
	public function init():Void
	{
		Social.i.addEventListener(IDNetEvent.ID_AUTH_COMPLETE, function(e:Event):Void {
			registerBtn.visible = false;
			registerBtn.removeEventListener(TouchEvent.TOUCH_END, _login);
			registerBtn.removeEventListener(MouseEvent.CLICK, _login);
			
			#if js
			status.text = "";
			//lgoutBtn.visible = false;
			return;
			#end
			trace("ID_AUTH_COMPLETE");
			status.text = "Welcome: " + Reg.userName;
			//For test
			//Social.i.logout();
		});
		
		if(Social.i.isAuthorized()) {
			Social.i.dispatch(IDNetEvent.ID_AUTH_COMPLETE);
		}
		
		Social.i.addEventListener(IDNetEvent.ID_AUTH_FAIL, function(e:Event):Void {
			status.text = "";
		});
		Social.i.addEventListener(IDNetEvent.ID_LOGOUT, function(e:Event):Void {
			status.text = "";
			//lgoutBtn.visible = false;
			
			registerBtn.visible = true;
			registerBtn.addEventListener(TouchEvent.TOUCH_END, _login);
			registerBtn.addEventListener(MouseEvent.CLICK, _login);
		});
	}
	
	private function onIDNetLogo(e:MouseEvent):Void 
	{
		Links.openURL("http://id.net");
	}
	
	private function onY8Logo(e:MouseEvent):Void 
	{
		Links.gotoLink("mainmenu_logo");
	}
	
	private function _login(e:MouseEvent):Void
	{
		popup.visible = false;
		
		Reg.soundManager.playSound("btnClick");
		
		if (Social.i.isAuthorized()) {
			#if js
			status.text = "";
			#end
			status.text = "Welcome: "+Reg.userName;
			registerBtn.visible = false;
			registerBtn.removeEventListener(TouchEvent.TOUCH_END, _login);
			registerBtn.removeEventListener(MouseEvent.CLICK, _login);
		} else {
			Social.i.loginPopup();
		}
	}
	
	private function _logout(e:MouseEvent):Void
	{
		#if js
		status.text = "";
		return;
		#end
		if (!Social.i.isAuthorized()) {
			status.text = '';
			//lgoutBtn.visible = false;
			registerBtn.visible = true;
			registerBtn.addEventListener(TouchEvent.TOUCH_END, _login);
			registerBtn.addEventListener(MouseEvent.CLICK, _login);
		} else {
			Social.i.logout();
		}
	}
}