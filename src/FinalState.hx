package ;
#if js
import js.JQuery;
import js.Lib;
#end
#if flash
import lib.encode.JPGEncoder;
#end
import haxe.crypto.Base64;
import haxe.crypto.BaseCode;
import haxe.Http;
import haxe.io.Bytes;
import haxe.io.BytesInput;
import idnet.common.events.IDNetEvent;
import idnet.Social;
import lime.graphics.format.PNG;
import lime.graphics.Image;
import lime.net.URLRequest;
import lime.project.Haxelib;
import openfl.events.Event;
import openfl.events.TouchEvent;
import openfl.net.URLLoader;
import openfl.net.URLRequestHeader;
import openfl.net.URLRequestMethod;
import openfl.net.URLVariables;
import lime.utils.ByteArray;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.BlendMode;
import openfl.display.JPEGEncoderOptions;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.Matrix;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.ui.Mouse;
import openfl.geom.Point;
import openfl.geom.Rectangle;
import ui.AlertPopup;
import ui.CustomButton;
import ui.CustomSprite;
import ui.FinalPopup;
import ui.MenuButton;
import ui.SoundButton;
import ui.UIConstructor;

/**
 * ...
 * @author Codir
 */
 
class FinalState extends Sprite
{
	private static inline var BASE_64_ENCODINGS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	private static inline var BASE_64_PADDING = "=";
	
	private var _name:String;
	private var uiConstructor:UIConstructor;
	private var doll:Doll;
	
	public var imageBtn:CustomSprite;
	public var imageOffBtn:CustomSprite;
	
	private var menuBtn:MenuButton;
	private var soundBtn:SoundButton;
	private var bg:CustomSprite;
	private var Y8Logo:CustomSprite;
	private var moreGames:CustomSprite;

	private var popupSprite:CustomSprite;
	private var loaderSprite:CustomSprite;
	private var popup:FinalPopup;
	
	private var loaderAnim:AnimatedSprite;
	
	public var status:TextField;
	public var picture:Bitmap;
	
	public function new() 
	{
		super();
		_name = "finalScreen_f";
		
		uiConstructor = new UIConstructor(this, _name+".json", "screens/"+_name);
		uiConstructor.createUI();
		
		bg = uiConstructor.uiElements.get("bg");
		
		popupSprite = uiConstructor.uiElements.get("empty_popup");
		popupSprite.visible = false;
		
		loaderSprite = uiConstructor.uiElements.get("loader");
		loaderSprite.visible = false;
		
		loaderAnim = new AnimatedSprite("loader/loader_f", 8, 0.5, true);
		loaderAnim.x = loaderSprite.x;
		loaderAnim.y = loaderSprite.y;
		loaderAnim.visible = false;
		
		this.addChild(loaderAnim);
		
		if (Social.i.isAuthorized())
		{
			popup = new FinalPopup(this, "Share your creations with friends by posting to your Y8 profile", uiConstructor);
		} else {
			popup = new FinalPopup(this, "Please login to save your creation on your y8 profile\nto share with your friends", uiConstructor, false, true);
		}
		this.addChild(popup);
		
		doll = uiConstructor.uiElements.get("doll");
		doll.setDresses();
		
		imageBtn = uiConstructor.uiElements.get("imageBtn");
		imageOffBtn = uiConstructor.uiElements.get("imageOffBtn");
		moreGames = uiConstructor.uiElements.get("moreGames");			
		soundBtn = uiConstructor.uiElements.get("soundBtn");
		menuBtn = uiConstructor.uiElements.get("menuBtn");
		Y8Logo = uiConstructor.uiElements.get("Y8_Logo");
		
		imageOffBtn.visible = false; 
		
		imageBtn.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		imageBtn.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		imageBtn.addEventListener(TouchEvent.TOUCH_END, popup.onClick);
		menuBtn.addEventListener(TouchEvent.TOUCH_END, menuBtnClick);
		Y8Logo.addEventListener(TouchEvent.TOUCH_END, onY8Logo);
		imageBtn.addEventListener(MouseEvent.CLICK, popup.onClick);
		menuBtn.addEventListener(MouseEvent.CLICK, menuBtnClick);
		Y8Logo.addEventListener(MouseEvent.CLICK, onY8Logo);
		
		#if flash
			if (!Links.isInnerDomain()) 
			{
				moreGames.buttonMode = true;
				Y8Logo.buttonMode = true;
			}
			imageBtn.buttonMode	 = true;
			soundBtn.buttonMode	 = true;
			menuBtn.buttonMode	 = true;
		#end
		
		if (Links.isInnerDomain()) 
		{
			moreGames.visible = false;
		}
		else
		{
			moreGames.addEventListener(TouchEvent.TOUCH_END, onMoreGames);
			moreGames.addEventListener(MouseEvent.CLICK, onMoreGames);
		}
		#if html5
		imageSendCallback(null);
		imageSendError(null);
		#end
		
		var myFormat:TextFormat = new TextFormat();
		
		myFormat.size = 22;
		
		status = new TextField();
		status.text = "";
		status.width = 300;
		status.height = 40;
		status.borderColor = 0x000000;
		status.selectable = false;
		status.textColor = 0x000000;
		status.alpha = 0.8;
		status.defaultTextFormat = myFormat;
		status.text = "status";
		status.x = 170;
		status.y = 480;
		addChild(status);
		
		if (Social.i.isAuthorized())
		{
			#if js
			status.text = "Logout not supported";
			#end
			status.text = "logged in: "+Reg.userName;
		}
		else
		{
			#if js
			status.text = "Logout not supported";
			#end
			status.text = "logged out";
		}
		
		Social.i.addEventListener(IDNetEvent.ID_AUTH_COMPLETE, function(e:Event):Void 
		{
			if (Social.i.isAuthorized())
			{
				#if js
				status.text = "Logout not supported";
				#end
				status.text = "logged in: " + Reg.userName;
				
				popup.textField.text = "Share your creations with friends by posting to your Y8 profile";
				popup._click = true;
				popup._register = false;
				popup.init();
			}
			else
			{
				#if js
				status.text = "Logout not supported";
				#end
				status.text = "logged out";
			}
		});
	}
	
	public function onMouseOver(e:MouseEvent):Void 
	{		
		popup.visible = true;
//		trace("FS: onMouseOver()");
	}
	
	public function onMouseOut(e:MouseEvent):Void 
	{		
		popup.visible = false;
//		trace("FS: onMouseOut()");
	}

	private function onMoreGames(e:MouseEvent):Void 
	{
		Links.gotoLink("moregames");
	}
	
	private function onY8Logo(e:MouseEvent):Void 
	{
		Links.gotoLink("mainmenu_logo");
	}
	
	private function menuBtnClick(e:MouseEvent):Void 
	{
		Reg.soundManager.playSound("btnClick");
		Main.stateManager.onQuit();
	}

	public function onImage(e:MouseEvent):Void
	{
		trace("FS: onImage()");
		imageBtn.visible = false;
		
		loaderAnim.visible = true;
		loaderAnim.play();
		
		if (Social.i.isAuthorized())
		{
			makePhoto();
		}
		else
		{
			trace("u are not authorized");
			Social.i.register();
			if (Social.i.isAuthorized())
			{
				makePhoto();
			}
		}
	}
	

	private function imageSendCallback(e:Dynamic):Void
	{
		if (e != null)
		{
			trace("FS: image send callback");
			
			loaderAnim.stop();
			loaderAnim.visible = false;
			
			imageBtn.visible = false;
			imageOffBtn.visible = true;

			imageBtn.removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		}
		else
		{
//			trace("for generation js code");
		}
	}
	
	private function imageSendError(e:Dynamic):Void
	{
		if (e != null)
		{
			trace("FS: image send error");
			
			loaderAnim.stop();
			loaderAnim.visible = false;
			imageBtn.visible = true;
			
			imageBtn.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		}
		else
		{
//			trace("for generation js code");
		}
	}
	
	private function getBitmap():BitmapData
	{
		var bitmap:BitmapData = new BitmapData(800, 533, true, 0xFF000000);
		var bgBitmap:BitmapData = new BitmapData(800, 533);
		var dollBitmap:BitmapData = doll.getBitmap();
		
		bitmap.draw(bg);
		bitmap.draw(dollBitmap, new Matrix(1, 0, 0, 1, doll.x, doll.y));
		
		return bitmap;
	}


	private function sendComlete(e:Event):Void 
	{
		imageSendCallback(e);
		Social.i.removeEventListener(IDNetEvent.ID_SEND_COMPLETE, sendComlete);
		Social.i.removeEventListener(IDNetEvent.ID_SEND_FAIL, sendFailed);
	}
	
	private function sendFailed(e:Event):Void 
	{
		imageSendCallback(e);
		Social.i.removeEventListener(IDNetEvent.ID_SEND_COMPLETE, sendComlete);
		Social.i.removeEventListener(IDNetEvent.ID_SEND_FAIL, sendFailed);
	}
	
	public function makePhoto():Void
	{
		picture = new Bitmap(getBitmap());
		#if flash
		var pictureSprite:Sprite = new Sprite();
		pictureSprite.graphics.beginBitmapFill(picture.bitmapData, new Matrix());
		pictureSprite.graphics.drawRect(picture.x, picture.y, picture.width, picture.height);
		pictureSprite.graphics.endFill();
		
		Social.i.addEventListener(IDNetEvent.ID_SEND_COMPLETE, sendComlete);
		Social.i.addEventListener(IDNetEvent.ID_SEND_FAIL, sendFailed);
		Social.i.sendImage(pictureSprite, 'jpg');
		#end		
		
		try
		{
			#if js
			addChild(picture);
			haxe.Timer.delay(sendHTML5, 100);
			#end
		}  
		catch ( unknown : Dynamic )
		{
		   trace("unknown exclusion in 'makePhoto()'; error: " + Std.string(unknown));
		}
	}
	
	public function sendHTML5():Void
	{
		var data: { session:String, client_id:String, picture:String };
		
		data = 
		{
			session: Reg.sessionKey,
			client_id: Reg.app_id,
			picture: ""
		};
		
		untyped __js__('var canvases = document.getElementsByTagName("canvas")');
		untyped __js__('var last = canvases.length');
		untyped __js__('var canvas = canvases[last - 1]');
		untyped __js__('data.picture = canvas.toDataURL("image/jpeg")');
		//untyped __js__('window.open(data.picture, "toDataURL() image", "width=800, height=533");');
		trace("Reg.postImagURL = " + Reg.postImagURL);
		untyped __js__("jQuery.post('https://www.id.net/api/user_data/profile2/appimage/', data, \u0024bind(this, this.imageSendCallback)).fail(\u0024bind(this, this.imageSendError))");
		picture.visible = false;
	}
}