package;


import haxe.Timer;
import haxe.Unserializer;
import lime.app.Preloader;
import lime.audio.openal.AL;
import lime.audio.AudioBuffer;
import lime.graphics.Font;
import lime.graphics.Image;
import lime.utils.ByteArray;
import lime.utils.UInt8Array;
import lime.Assets;

#if (sys || nodejs)
import sys.FileSystem;
#end

#if flash
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.events.Event;
import flash.media.Sound;
import flash.net.URLLoader;
import flash.net.URLRequest;
#end


class DefaultAssetLibrary extends AssetLibrary {
	
	
	public var className (default, null) = new Map <String, Dynamic> ();
	public var path (default, null) = new Map <String, String> ();
	public var type (default, null) = new Map <String, AssetType> ();
	
	private var lastModified:Float;
	private var timer:Timer;
	
	
	public function new () {
		
		super ();
		
		#if flash
		
		className.set ("assets/clock.json", __ASSET__assets_clock_json);
		type.set ("assets/clock.json", AssetType.TEXT);
		className.set ("assets/doll_f.json", __ASSET__assets_doll_f_json);
		type.set ("assets/doll_f.json", AssetType.TEXT);
		className.set ("assets/doll_small_f.json", __ASSET__assets_doll_small_f_json);
		type.set ("assets/doll_small_f.json", AssetType.TEXT);
		className.set ("assets/finalScreen_f.json", __ASSET__assets_finalscreen_f_json);
		type.set ("assets/finalScreen_f.json", AssetType.TEXT);
		className.set ("assets/loader/loader_f.json", __ASSET__assets_loader_loader_f_json);
		type.set ("assets/loader/loader_f.json", AssetType.TEXT);
		className.set ("assets/loader/loader_f.png", __ASSET__assets_loader_loader_f_png);
		type.set ("assets/loader/loader_f.png", AssetType.IMAGE);
		className.set ("assets/masks/mask_f_1.json", __ASSET__assets_masks_mask_f_1_json);
		type.set ("assets/masks/mask_f_1.json", AssetType.TEXT);
		className.set ("assets/masks/mask_f_1.png", __ASSET__assets_masks_mask_f_1_png);
		type.set ("assets/masks/mask_f_1.png", AssetType.IMAGE);
		className.set ("assets/masks/mask_f_2.json", __ASSET__assets_masks_mask_f_2_json);
		type.set ("assets/masks/mask_f_2.json", AssetType.TEXT);
		className.set ("assets/masks/mask_f_2.png", __ASSET__assets_masks_mask_f_2_png);
		type.set ("assets/masks/mask_f_2.png", AssetType.IMAGE);
		className.set ("assets/masks/mask_f_3.json", __ASSET__assets_masks_mask_f_3_json);
		type.set ("assets/masks/mask_f_3.json", AssetType.TEXT);
		className.set ("assets/masks/mask_f_3.png", __ASSET__assets_masks_mask_f_3_png);
		type.set ("assets/masks/mask_f_3.png", AssetType.IMAGE);
		className.set ("assets/menu_f.json", __ASSET__assets_menu_f_json);
		type.set ("assets/menu_f.json", AssetType.TEXT);
		className.set ("assets/music/button_click_1.mp3", __ASSET__assets_music_button_click_1_mp3);
		type.set ("assets/music/button_click_1.mp3", AssetType.MUSIC);
		className.set ("assets/music/button_click_1.ogg", __ASSET__assets_music_button_click_1_ogg);
		type.set ("assets/music/button_click_1.ogg", AssetType.SOUND);
		className.set ("assets/music/music_fairy.mp3", __ASSET__assets_music_music_fairy_mp3);
		type.set ("assets/music/music_fairy.mp3", AssetType.MUSIC);
		className.set ("assets/music/music_fairy.ogg", __ASSET__assets_music_music_fairy_ogg);
		type.set ("assets/music/music_fairy.ogg", AssetType.SOUND);
		className.set ("assets/pauseMenu_f.json", __ASSET__assets_pausemenu_f_json);
		type.set ("assets/pauseMenu_f.json", AssetType.TEXT);
		className.set ("assets/preloader/flash/fml_preloader.swf", __ASSET__assets_preloader_flash_fml_preloader_swf);
		type.set ("assets/preloader/flash/fml_preloader.swf", AssetType.BINARY);
		className.set ("assets/preloader/html5/fairy_preloader_bg.png", __ASSET__assets_preloader_html5_fairy_preloader_bg_png);
		type.set ("assets/preloader/html5/fairy_preloader_bg.png", AssetType.IMAGE);
		className.set ("assets/preloader/html5/id_net_logo.png", __ASSET__assets_preloader_html5_id_net_logo_png);
		type.set ("assets/preloader/html5/id_net_logo.png", AssetType.IMAGE);
		className.set ("assets/preloader/html5/Y8_logo.png", __ASSET__assets_preloader_html5_y8_logo_png);
		type.set ("assets/preloader/html5/Y8_logo.png", AssetType.IMAGE);
		className.set ("assets/screen1_f.json", __ASSET__assets_screen1_f_json);
		type.set ("assets/screen1_f.json", AssetType.TEXT);
		className.set ("assets/screen1_f_bck.json", __ASSET__assets_screen1_f_bck_json);
		type.set ("assets/screen1_f_bck.json", AssetType.TEXT);
		className.set ("assets/screen2_f.json", __ASSET__assets_screen2_f_json);
		type.set ("assets/screen2_f.json", AssetType.TEXT);
		className.set ("assets/screen3_f.json", __ASSET__assets_screen3_f_json);
		type.set ("assets/screen3_f.json", AssetType.TEXT);
		className.set ("assets/screens/doll_f.json", __ASSET__assets_screens_doll_f_json);
		type.set ("assets/screens/doll_f.json", AssetType.TEXT);
		className.set ("assets/screens/doll_f.png", __ASSET__assets_screens_doll_f_png);
		type.set ("assets/screens/doll_f.png", AssetType.IMAGE);
		className.set ("assets/screens/doll_small_f.json", __ASSET__assets_screens_doll_small_f_json);
		type.set ("assets/screens/doll_small_f.json", AssetType.TEXT);
		className.set ("assets/screens/doll_small_f.png", __ASSET__assets_screens_doll_small_f_png);
		type.set ("assets/screens/doll_small_f.png", AssetType.IMAGE);
		className.set ("assets/screens/finalScreen_f.json", __ASSET__assets_screens_finalscreen_f_json);
		type.set ("assets/screens/finalScreen_f.json", AssetType.TEXT);
		className.set ("assets/screens/finalScreen_f.png", __ASSET__assets_screens_finalscreen_f_png);
		type.set ("assets/screens/finalScreen_f.png", AssetType.IMAGE);
		className.set ("assets/screens/menu_f.json", __ASSET__assets_screens_menu_f_json);
		type.set ("assets/screens/menu_f.json", AssetType.TEXT);
		className.set ("assets/screens/menu_f.png", __ASSET__assets_screens_menu_f_png);
		type.set ("assets/screens/menu_f.png", AssetType.IMAGE);
		className.set ("assets/screens/pauseMenu_f.json", __ASSET__assets_screens_pausemenu_f_json);
		type.set ("assets/screens/pauseMenu_f.json", AssetType.TEXT);
		className.set ("assets/screens/pauseMenu_f.png", __ASSET__assets_screens_pausemenu_f_png);
		type.set ("assets/screens/pauseMenu_f.png", AssetType.IMAGE);
		className.set ("assets/screens/screen1_f.json", __ASSET__assets_screens_screen1_f_json);
		type.set ("assets/screens/screen1_f.json", AssetType.TEXT);
		className.set ("assets/screens/screen1_f.png", __ASSET__assets_screens_screen1_f_png);
		type.set ("assets/screens/screen1_f.png", AssetType.IMAGE);
		className.set ("assets/screens/screen2_f.json", __ASSET__assets_screens_screen2_f_json);
		type.set ("assets/screens/screen2_f.json", AssetType.TEXT);
		className.set ("assets/screens/screen2_f.png", __ASSET__assets_screens_screen2_f_png);
		type.set ("assets/screens/screen2_f.png", AssetType.IMAGE);
		className.set ("assets/screens/screen3_f.json", __ASSET__assets_screens_screen3_f_json);
		type.set ("assets/screens/screen3_f.json", AssetType.TEXT);
		className.set ("assets/screens/screen3_f.png", __ASSET__assets_screens_screen3_f_png);
		type.set ("assets/screens/screen3_f.png", AssetType.IMAGE);
		className.set ("assets/sliders/accessory.json", __ASSET__assets_sliders_accessory_json);
		type.set ("assets/sliders/accessory.json", AssetType.TEXT);
		className.set ("assets/sliders/accessory.png", __ASSET__assets_sliders_accessory_png);
		type.set ("assets/sliders/accessory.png", AssetType.IMAGE);
		className.set ("assets/sliders/brows.json", __ASSET__assets_sliders_brows_json);
		type.set ("assets/sliders/brows.json", AssetType.TEXT);
		className.set ("assets/sliders/brows.png", __ASSET__assets_sliders_brows_png);
		type.set ("assets/sliders/brows.png", AssetType.IMAGE);
		className.set ("assets/sliders/cloth_bottom.json", __ASSET__assets_sliders_cloth_bottom_json);
		type.set ("assets/sliders/cloth_bottom.json", AssetType.TEXT);
		className.set ("assets/sliders/cloth_bottom.png", __ASSET__assets_sliders_cloth_bottom_png);
		type.set ("assets/sliders/cloth_bottom.png", AssetType.IMAGE);
		className.set ("assets/sliders/cloth_top.json", __ASSET__assets_sliders_cloth_top_json);
		type.set ("assets/sliders/cloth_top.json", AssetType.TEXT);
		className.set ("assets/sliders/cloth_top.png", __ASSET__assets_sliders_cloth_top_png);
		type.set ("assets/sliders/cloth_top.png", AssetType.IMAGE);
		className.set ("assets/sliders/eyes.json", __ASSET__assets_sliders_eyes_json);
		type.set ("assets/sliders/eyes.json", AssetType.TEXT);
		className.set ("assets/sliders/eyes.png", __ASSET__assets_sliders_eyes_png);
		type.set ("assets/sliders/eyes.png", AssetType.IMAGE);
		className.set ("assets/sliders/hairs.json", __ASSET__assets_sliders_hairs_json);
		type.set ("assets/sliders/hairs.json", AssetType.TEXT);
		className.set ("assets/sliders/hairs.png", __ASSET__assets_sliders_hairs_png);
		type.set ("assets/sliders/hairs.png", AssetType.IMAGE);
		className.set ("assets/sliders/ink.json", __ASSET__assets_sliders_ink_json);
		type.set ("assets/sliders/ink.json", AssetType.TEXT);
		className.set ("assets/sliders/ink.png", __ASSET__assets_sliders_ink_png);
		type.set ("assets/sliders/ink.png", AssetType.IMAGE);
		className.set ("assets/sliders/jewelry.json", __ASSET__assets_sliders_jewelry_json);
		type.set ("assets/sliders/jewelry.json", AssetType.TEXT);
		className.set ("assets/sliders/jewelry.png", __ASSET__assets_sliders_jewelry_png);
		type.set ("assets/sliders/jewelry.png", AssetType.IMAGE);
		className.set ("assets/sliders/pomade.json", __ASSET__assets_sliders_pomade_json);
		type.set ("assets/sliders/pomade.json", AssetType.TEXT);
		className.set ("assets/sliders/pomade.png", __ASSET__assets_sliders_pomade_png);
		type.set ("assets/sliders/pomade.png", AssetType.IMAGE);
		className.set ("assets/sliders/rouge.json", __ASSET__assets_sliders_rouge_json);
		type.set ("assets/sliders/rouge.json", AssetType.TEXT);
		className.set ("assets/sliders/rouge.png", __ASSET__assets_sliders_rouge_png);
		type.set ("assets/sliders/rouge.png", AssetType.IMAGE);
		className.set ("assets/sliders/shadows.json", __ASSET__assets_sliders_shadows_json);
		type.set ("assets/sliders/shadows.json", AssetType.TEXT);
		className.set ("assets/sliders/shadows.png", __ASSET__assets_sliders_shadows_png);
		type.set ("assets/sliders/shadows.png", AssetType.IMAGE);
		className.set ("assets/sliders/wings.json", __ASSET__assets_sliders_wings_json);
		type.set ("assets/sliders/wings.json", AssetType.TEXT);
		className.set ("assets/sliders/wings.png", __ASSET__assets_sliders_wings_png);
		type.set ("assets/sliders/wings.png", AssetType.IMAGE);
		className.set ("music", __ASSET__assets_music_music_fairy_mp4);
		type.set ("music", AssetType.SOUND);
		className.set ("btnClick", __ASSET__assets_music_button_click_2);
		type.set ("btnClick", AssetType.SOUND);
		
		
		#elseif html5
		
		var id;
		id = "assets/clock.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/doll_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/doll_small_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/finalScreen_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/loader/loader_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/loader/loader_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/masks/mask_f_1.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/masks/mask_f_1.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/masks/mask_f_2.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/masks/mask_f_2.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/masks/mask_f_3.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/masks/mask_f_3.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/menu_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/music/button_click_1.mp3";
		path.set (id, id);
		
		type.set (id, AssetType.MUSIC);
		id = "assets/music/button_click_1.ogg";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/music/music_fairy.mp3";
		path.set (id, id);
		
		type.set (id, AssetType.MUSIC);
		id = "assets/music/music_fairy.ogg";
		path.set (id, id);
		
		type.set (id, AssetType.SOUND);
		id = "assets/pauseMenu_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/preloader/flash/fml_preloader.swf";
		path.set (id, id);
		
		type.set (id, AssetType.BINARY);
		id = "assets/preloader/html5/fairy_preloader_bg.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/preloader/html5/id_net_logo.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/preloader/html5/Y8_logo.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screen1_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screen1_f_bck.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screen2_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screen3_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/doll_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/doll_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/doll_small_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/doll_small_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/finalScreen_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/finalScreen_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/menu_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/menu_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/pauseMenu_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/pauseMenu_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/screen1_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/screen1_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/screen2_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/screen2_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/screens/screen3_f.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/screens/screen3_f.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/accessory.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/accessory.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/brows.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/brows.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/cloth_bottom.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/cloth_bottom.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/cloth_top.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/cloth_top.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/eyes.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/eyes.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/hairs.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/hairs.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/ink.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/ink.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/jewelry.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/jewelry.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/pomade.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/pomade.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/rouge.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/rouge.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/shadows.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/shadows.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "assets/sliders/wings.json";
		path.set (id, id);
		
		type.set (id, AssetType.TEXT);
		id = "assets/sliders/wings.png";
		path.set (id, id);
		
		type.set (id, AssetType.IMAGE);
		id = "music";
		path.set (id, "assets/music/music_fairy.mp3");
		
		type.set (id, AssetType.SOUND);
		id = "btnClick";
		path.set (id, "assets/music/button_click_1.mp3");
		
		type.set (id, AssetType.SOUND);
		
		
		var assetsPrefix = ApplicationMain.config.assetsPrefix;
		if (assetsPrefix != null) {
			for (k in path.keys()) {
				path.set(k, assetsPrefix + path[k]);
			}
		}
		
		#else
		
		#if openfl
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		#end
		
		#if (windows || mac || linux)
		
		var useManifest = false;
		
		className.set ("assets/clock.json", __ASSET__assets_clock_json);
		type.set ("assets/clock.json", AssetType.TEXT);
		
		className.set ("assets/doll_f.json", __ASSET__assets_doll_f_json);
		type.set ("assets/doll_f.json", AssetType.TEXT);
		
		className.set ("assets/doll_small_f.json", __ASSET__assets_doll_small_f_json);
		type.set ("assets/doll_small_f.json", AssetType.TEXT);
		
		className.set ("assets/finalScreen_f.json", __ASSET__assets_finalscreen_f_json);
		type.set ("assets/finalScreen_f.json", AssetType.TEXT);
		
		className.set ("assets/loader/loader_f.json", __ASSET__assets_loader_loader_f_json);
		type.set ("assets/loader/loader_f.json", AssetType.TEXT);
		
		className.set ("assets/loader/loader_f.png", __ASSET__assets_loader_loader_f_png);
		type.set ("assets/loader/loader_f.png", AssetType.IMAGE);
		
		className.set ("assets/masks/mask_f_1.json", __ASSET__assets_masks_mask_f_1_json);
		type.set ("assets/masks/mask_f_1.json", AssetType.TEXT);
		
		className.set ("assets/masks/mask_f_1.png", __ASSET__assets_masks_mask_f_1_png);
		type.set ("assets/masks/mask_f_1.png", AssetType.IMAGE);
		
		className.set ("assets/masks/mask_f_2.json", __ASSET__assets_masks_mask_f_2_json);
		type.set ("assets/masks/mask_f_2.json", AssetType.TEXT);
		
		className.set ("assets/masks/mask_f_2.png", __ASSET__assets_masks_mask_f_2_png);
		type.set ("assets/masks/mask_f_2.png", AssetType.IMAGE);
		
		className.set ("assets/masks/mask_f_3.json", __ASSET__assets_masks_mask_f_3_json);
		type.set ("assets/masks/mask_f_3.json", AssetType.TEXT);
		
		className.set ("assets/masks/mask_f_3.png", __ASSET__assets_masks_mask_f_3_png);
		type.set ("assets/masks/mask_f_3.png", AssetType.IMAGE);
		
		className.set ("assets/menu_f.json", __ASSET__assets_menu_f_json);
		type.set ("assets/menu_f.json", AssetType.TEXT);
		
		className.set ("assets/music/button_click_1.mp3", __ASSET__assets_music_button_click_1_mp3);
		type.set ("assets/music/button_click_1.mp3", AssetType.MUSIC);
		
		className.set ("assets/music/button_click_1.ogg", __ASSET__assets_music_button_click_1_ogg);
		type.set ("assets/music/button_click_1.ogg", AssetType.SOUND);
		
		className.set ("assets/music/music_fairy.mp3", __ASSET__assets_music_music_fairy_mp3);
		type.set ("assets/music/music_fairy.mp3", AssetType.MUSIC);
		
		className.set ("assets/music/music_fairy.ogg", __ASSET__assets_music_music_fairy_ogg);
		type.set ("assets/music/music_fairy.ogg", AssetType.SOUND);
		
		className.set ("assets/pauseMenu_f.json", __ASSET__assets_pausemenu_f_json);
		type.set ("assets/pauseMenu_f.json", AssetType.TEXT);
		
		className.set ("assets/preloader/flash/fml_preloader.swf", __ASSET__assets_preloader_flash_fml_preloader_swf);
		type.set ("assets/preloader/flash/fml_preloader.swf", AssetType.BINARY);
		
		className.set ("assets/preloader/html5/fairy_preloader_bg.png", __ASSET__assets_preloader_html5_fairy_preloader_bg_png);
		type.set ("assets/preloader/html5/fairy_preloader_bg.png", AssetType.IMAGE);
		
		className.set ("assets/preloader/html5/id_net_logo.png", __ASSET__assets_preloader_html5_id_net_logo_png);
		type.set ("assets/preloader/html5/id_net_logo.png", AssetType.IMAGE);
		
		className.set ("assets/preloader/html5/Y8_logo.png", __ASSET__assets_preloader_html5_y8_logo_png);
		type.set ("assets/preloader/html5/Y8_logo.png", AssetType.IMAGE);
		
		className.set ("assets/screen1_f.json", __ASSET__assets_screen1_f_json);
		type.set ("assets/screen1_f.json", AssetType.TEXT);
		
		className.set ("assets/screen1_f_bck.json", __ASSET__assets_screen1_f_bck_json);
		type.set ("assets/screen1_f_bck.json", AssetType.TEXT);
		
		className.set ("assets/screen2_f.json", __ASSET__assets_screen2_f_json);
		type.set ("assets/screen2_f.json", AssetType.TEXT);
		
		className.set ("assets/screen3_f.json", __ASSET__assets_screen3_f_json);
		type.set ("assets/screen3_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/doll_f.json", __ASSET__assets_screens_doll_f_json);
		type.set ("assets/screens/doll_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/doll_f.png", __ASSET__assets_screens_doll_f_png);
		type.set ("assets/screens/doll_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/doll_small_f.json", __ASSET__assets_screens_doll_small_f_json);
		type.set ("assets/screens/doll_small_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/doll_small_f.png", __ASSET__assets_screens_doll_small_f_png);
		type.set ("assets/screens/doll_small_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/finalScreen_f.json", __ASSET__assets_screens_finalscreen_f_json);
		type.set ("assets/screens/finalScreen_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/finalScreen_f.png", __ASSET__assets_screens_finalscreen_f_png);
		type.set ("assets/screens/finalScreen_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/menu_f.json", __ASSET__assets_screens_menu_f_json);
		type.set ("assets/screens/menu_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/menu_f.png", __ASSET__assets_screens_menu_f_png);
		type.set ("assets/screens/menu_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/pauseMenu_f.json", __ASSET__assets_screens_pausemenu_f_json);
		type.set ("assets/screens/pauseMenu_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/pauseMenu_f.png", __ASSET__assets_screens_pausemenu_f_png);
		type.set ("assets/screens/pauseMenu_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/screen1_f.json", __ASSET__assets_screens_screen1_f_json);
		type.set ("assets/screens/screen1_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/screen1_f.png", __ASSET__assets_screens_screen1_f_png);
		type.set ("assets/screens/screen1_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/screen2_f.json", __ASSET__assets_screens_screen2_f_json);
		type.set ("assets/screens/screen2_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/screen2_f.png", __ASSET__assets_screens_screen2_f_png);
		type.set ("assets/screens/screen2_f.png", AssetType.IMAGE);
		
		className.set ("assets/screens/screen3_f.json", __ASSET__assets_screens_screen3_f_json);
		type.set ("assets/screens/screen3_f.json", AssetType.TEXT);
		
		className.set ("assets/screens/screen3_f.png", __ASSET__assets_screens_screen3_f_png);
		type.set ("assets/screens/screen3_f.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/accessory.json", __ASSET__assets_sliders_accessory_json);
		type.set ("assets/sliders/accessory.json", AssetType.TEXT);
		
		className.set ("assets/sliders/accessory.png", __ASSET__assets_sliders_accessory_png);
		type.set ("assets/sliders/accessory.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/brows.json", __ASSET__assets_sliders_brows_json);
		type.set ("assets/sliders/brows.json", AssetType.TEXT);
		
		className.set ("assets/sliders/brows.png", __ASSET__assets_sliders_brows_png);
		type.set ("assets/sliders/brows.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/cloth_bottom.json", __ASSET__assets_sliders_cloth_bottom_json);
		type.set ("assets/sliders/cloth_bottom.json", AssetType.TEXT);
		
		className.set ("assets/sliders/cloth_bottom.png", __ASSET__assets_sliders_cloth_bottom_png);
		type.set ("assets/sliders/cloth_bottom.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/cloth_top.json", __ASSET__assets_sliders_cloth_top_json);
		type.set ("assets/sliders/cloth_top.json", AssetType.TEXT);
		
		className.set ("assets/sliders/cloth_top.png", __ASSET__assets_sliders_cloth_top_png);
		type.set ("assets/sliders/cloth_top.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/eyes.json", __ASSET__assets_sliders_eyes_json);
		type.set ("assets/sliders/eyes.json", AssetType.TEXT);
		
		className.set ("assets/sliders/eyes.png", __ASSET__assets_sliders_eyes_png);
		type.set ("assets/sliders/eyes.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/hairs.json", __ASSET__assets_sliders_hairs_json);
		type.set ("assets/sliders/hairs.json", AssetType.TEXT);
		
		className.set ("assets/sliders/hairs.png", __ASSET__assets_sliders_hairs_png);
		type.set ("assets/sliders/hairs.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/ink.json", __ASSET__assets_sliders_ink_json);
		type.set ("assets/sliders/ink.json", AssetType.TEXT);
		
		className.set ("assets/sliders/ink.png", __ASSET__assets_sliders_ink_png);
		type.set ("assets/sliders/ink.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/jewelry.json", __ASSET__assets_sliders_jewelry_json);
		type.set ("assets/sliders/jewelry.json", AssetType.TEXT);
		
		className.set ("assets/sliders/jewelry.png", __ASSET__assets_sliders_jewelry_png);
		type.set ("assets/sliders/jewelry.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/pomade.json", __ASSET__assets_sliders_pomade_json);
		type.set ("assets/sliders/pomade.json", AssetType.TEXT);
		
		className.set ("assets/sliders/pomade.png", __ASSET__assets_sliders_pomade_png);
		type.set ("assets/sliders/pomade.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/rouge.json", __ASSET__assets_sliders_rouge_json);
		type.set ("assets/sliders/rouge.json", AssetType.TEXT);
		
		className.set ("assets/sliders/rouge.png", __ASSET__assets_sliders_rouge_png);
		type.set ("assets/sliders/rouge.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/shadows.json", __ASSET__assets_sliders_shadows_json);
		type.set ("assets/sliders/shadows.json", AssetType.TEXT);
		
		className.set ("assets/sliders/shadows.png", __ASSET__assets_sliders_shadows_png);
		type.set ("assets/sliders/shadows.png", AssetType.IMAGE);
		
		className.set ("assets/sliders/wings.json", __ASSET__assets_sliders_wings_json);
		type.set ("assets/sliders/wings.json", AssetType.TEXT);
		
		className.set ("assets/sliders/wings.png", __ASSET__assets_sliders_wings_png);
		type.set ("assets/sliders/wings.png", AssetType.IMAGE);
		
		className.set ("music", __ASSET__assets_music_music_fairy_mp4);
		type.set ("music", AssetType.SOUND);
		
		className.set ("btnClick", __ASSET__assets_music_button_click_2);
		type.set ("btnClick", AssetType.SOUND);
		
		
		if (useManifest) {
			
			loadManifest ();
			
			if (Sys.args ().indexOf ("-livereload") > -1) {
				
				var path = FileSystem.fullPath ("manifest");
				lastModified = FileSystem.stat (path).mtime.getTime ();
				
				timer = new Timer (2000);
				timer.run = function () {
					
					var modified = FileSystem.stat (path).mtime.getTime ();
					
					if (modified > lastModified) {
						
						lastModified = modified;
						loadManifest ();
						
						if (eventCallback != null) {
							
							eventCallback (this, "change");
							
						}
						
					}
					
				}
				
			}
			
		}
		
		#else
		
		loadManifest ();
		
		#end
		#end
		
	}
	
	
	public override function exists (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var assetType = this.type.get (id);
		
		if (assetType != null) {
			
			if (assetType == requestedType || ((requestedType == SOUND || requestedType == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if ((assetType == BINARY || assetType == TEXT) && requestedType == BINARY) {
				
				return true;
				
			} else if (path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (requestedType == BINARY || requestedType == null || (assetType == BINARY && requestedType == TEXT)) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getAudioBuffer (id:String):AudioBuffer {
		
		#if flash
		
		var buffer = new AudioBuffer ();
		buffer.src = cast (Type.createInstance (className.get (id), []), Sound);
		return buffer;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		return AudioBuffer.fromFile (path.get (id));
		//if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		//else return new Sound (new URLRequest (path.get (id)), null, type.get (id) == MUSIC);
		
		#end
		
	}
	
	
	public override function getBytes (id:String):ByteArray {
		
		#if flash
		
		return cast (Type.createInstance (className.get (id), []), ByteArray);
		
		#elseif html5
		
		var bytes:ByteArray = null;
		var data = Preloader.loaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			bytes = new ByteArray ();
			bytes.writeUTFBytes (data);
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), ByteArray);
		else return ByteArray.readFile (path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Dynamic /*Font*/ {
		
		// TODO: Complete Lime Font API
		
		#if openfl
		#if (flash || js)
		
		return cast (Type.createInstance (className.get (id), []), openfl.text.Font);
		
		#else
		
		if (className.exists (id)) {
			
			var fontClass = className.get (id);
			openfl.text.Font.registerFont (fontClass);
			return cast (Type.createInstance (fontClass, []), openfl.text.Font);
			
		} else {
			
			return new openfl.text.Font (path.get (id));
			
		}
		
		#end
		#end
		
		return null;
		
	}
	
	
	public override function getImage (id:String):Image {
		
		#if flash
		
		return Image.fromBitmapData (cast (Type.createInstance (className.get (id), []), BitmapData));
		
		#elseif html5
		
		return Image.fromImageElement (Preloader.images.get (path.get (id)));
		
		#else
		
		return Image.fromFile (path.get (id));
		
		#end
		
	}
	
	
	/*public override function getMusic (id:String):Dynamic {
		
		#if flash
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		//var sound = new Sound ();
		//sound.__buffer = true;
		//sound.load (new URLRequest (path.get (id)));
		//return sound;
		return null;
		
		#elseif html5
		
		return null;
		//return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		return null;
		//if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		//else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}*/
	
	
	public override function getPath (id:String):String {
		
		//#if ios
		
		//return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		//#else
		
		return path.get (id);
		
		//#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if html5
		
		var bytes:ByteArray = null;
		var data = Preloader.loaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			return cast data;
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes.readUTFBytes (bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.readUTFBytes (bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:String):Bool {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		
		#if flash
		
		if (requestedType != AssetType.MUSIC && requestedType != AssetType.SOUND) {
			
			return className.exists (id);
			
		}
		
		#end
		
		return true;
		
	}
	
	
	public override function list (type:String):Array<String> {
		
		var requestedType = type != null ? cast (type, AssetType) : null;
		var items = [];
		
		for (id in this.type.keys ()) {
			
			if (requestedType == null || exists (id, type)) {
				
				items.push (id);
				
			}
			
		}
		
		return items;
		
	}
	
	
	public override function loadAudioBuffer (id:String, handler:AudioBuffer -> Void):Void {
		
		#if (flash || js)
		
		//if (path.exists (id)) {
			
		//	var loader = new Loader ();
		//	loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
		//		handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
		//	});
		//	loader.load (new URLRequest (path.get (id)));
			
		//} else {
			
			handler (getAudioBuffer (id));
			
		//}
		
		#else
		
		handler (getAudioBuffer (id));
		
		#end
		
	}
	
	
	public override function loadBytes (id:String, handler:ByteArray -> Void):Void {
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = new ByteArray ();
				bytes.writeUTFBytes (event.currentTarget.data);
				bytes.position = 0;
				
				handler (bytes);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBytes (id));
			
		}
		
		#else
		
		handler (getBytes (id));
		
		#end
		
	}
	
	
	public override function loadImage (id:String, handler:Image -> Void):Void {
		
		#if flash
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bitmapData = cast (event.currentTarget.content, Bitmap).bitmapData;
				handler (Image.fromBitmapData (bitmapData));
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getImage (id));
			
		}
		
		#else
		
		handler (getImage (id));
		
		#end
		
	}
	
	
	#if (!flash && !html5)
	private function loadManifest ():Void {
		
		try {
			
			#if blackberry
			var bytes = ByteArray.readFile ("app/native/manifest");
			#elseif tizen
			var bytes = ByteArray.readFile ("../res/manifest");
			#elseif emscripten
			var bytes = ByteArray.readFile ("assets/manifest");
			#elseif (mac && java)
			var bytes = ByteArray.readFile ("../Resources/manifest");
			#else
			var bytes = ByteArray.readFile ("manifest");
			#end
			
			if (bytes != null) {
				
				bytes.position = 0;
				
				if (bytes.length > 0) {
					
					var data = bytes.readUTFBytes (bytes.length);
					
					if (data != null && data.length > 0) {
						
						var manifest:Array<Dynamic> = Unserializer.run (data);
						
						for (asset in manifest) {
							
							if (!className.exists (asset.id)) {
								
								path.set (asset.id, asset.path);
								type.set (asset.id, cast (asset.type, AssetType));
								
							}
							
						}
						
					}
					
				}
				
			} else {
				
				trace ("Warning: Could not load asset manifest (bytes was null)");
				
			}
		
		} catch (e:Dynamic) {
			
			trace ('Warning: Could not load asset manifest (${e})');
			
		}
		
	}
	#end
	
	
	/*public override function loadMusic (id:String, handler:Dynamic -> Void):Void {
		
		#if (flash || js)
		
		//if (path.exists (id)) {
			
		//	var loader = new Loader ();
		//	loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
		//		handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
		//	});
		//	loader.load (new URLRequest (path.get (id)));
			
		//} else {
			
			handler (getMusic (id));
			
		//}
		
		#else
		
		handler (getMusic (id));
		
		#end
		
	}*/
	
	
	public override function loadText (id:String, handler:String -> Void):Void {
		
		//#if html5
		
		/*if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (event.currentTarget.data);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getText (id));
			
		}*/
		
		//#else
		
		var callback = function (bytes:ByteArray):Void {
			
			if (bytes == null) {
				
				handler (null);
				
			} else {
				
				handler (bytes.readUTFBytes (bytes.length));
				
			}
			
		}
		
		loadBytes (id, callback);
		
		//#end
		
	}
	
	
}


#if !display
#if flash

@:keep @:bind #if display private #end class __ASSET__assets_clock_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_doll_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_doll_small_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_finalscreen_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_loader_loader_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_loader_loader_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_1_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_2_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_2_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_3_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_masks_mask_f_3_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_menu_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_music_button_click_1_mp3 extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_music_button_click_1_ogg extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_music_music_fairy_mp3 extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_music_music_fairy_ogg extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_pausemenu_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_preloader_flash_fml_preloader_swf extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_preloader_html5_fairy_preloader_bg_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_preloader_html5_id_net_logo_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_preloader_html5_y8_logo_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screen1_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screen1_f_bck_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screen2_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screen3_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_doll_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_doll_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_doll_small_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_doll_small_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_finalscreen_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_finalscreen_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_menu_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_menu_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_pausemenu_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_pausemenu_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen1_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen1_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen2_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen2_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen3_f_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_screens_screen3_f_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_accessory_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_accessory_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_brows_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_brows_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_cloth_bottom_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_cloth_bottom_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_cloth_top_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_cloth_top_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_eyes_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_eyes_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_hairs_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_hairs_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_ink_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_ink_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_jewelry_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_jewelry_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_pomade_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_pomade_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_rouge_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_rouge_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_shadows_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_shadows_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_wings_json extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_sliders_wings_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep @:bind #if display private #end class __ASSET__assets_music_music_fairy_mp4 extends null { }
@:keep @:bind #if display private #end class __ASSET__assets_music_button_click_2 extends null { }


#elseif html5

#if openfl





































































#end

#else

#if openfl

#end

#if (windows || mac || linux)


@:file("assets/clock.json") class __ASSET__assets_clock_json extends lime.utils.ByteArray {}
@:file("assets/doll_f.json") class __ASSET__assets_doll_f_json extends lime.utils.ByteArray {}
@:file("assets/doll_small_f.json") class __ASSET__assets_doll_small_f_json extends lime.utils.ByteArray {}
@:file("assets/finalScreen_f.json") class __ASSET__assets_finalscreen_f_json extends lime.utils.ByteArray {}
@:file("assets/loader/loader_f.json") class __ASSET__assets_loader_loader_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/loader/loader_f.png") class __ASSET__assets_loader_loader_f_png extends lime.graphics.Image {}
@:file("assets/masks/mask_f_1.json") class __ASSET__assets_masks_mask_f_1_json extends lime.utils.ByteArray {}
@:bitmap("assets/masks/mask_f_1.png") class __ASSET__assets_masks_mask_f_1_png extends lime.graphics.Image {}
@:file("assets/masks/mask_f_2.json") class __ASSET__assets_masks_mask_f_2_json extends lime.utils.ByteArray {}
@:bitmap("assets/masks/mask_f_2.png") class __ASSET__assets_masks_mask_f_2_png extends lime.graphics.Image {}
@:file("assets/masks/mask_f_3.json") class __ASSET__assets_masks_mask_f_3_json extends lime.utils.ByteArray {}
@:bitmap("assets/masks/mask_f_3.png") class __ASSET__assets_masks_mask_f_3_png extends lime.graphics.Image {}
@:file("assets/menu_f.json") class __ASSET__assets_menu_f_json extends lime.utils.ByteArray {}
@:sound("assets/music/button_click_1.mp3") class __ASSET__assets_music_button_click_1_mp3 extends lime.audio.AudioSource {}
@:sound("assets/music/button_click_1.ogg") class __ASSET__assets_music_button_click_1_ogg extends lime.audio.AudioSource {}
@:sound("assets/music/music_fairy.mp3") class __ASSET__assets_music_music_fairy_mp3 extends lime.audio.AudioSource {}
@:sound("assets/music/music_fairy.ogg") class __ASSET__assets_music_music_fairy_ogg extends lime.audio.AudioSource {}
@:file("assets/pauseMenu_f.json") class __ASSET__assets_pausemenu_f_json extends lime.utils.ByteArray {}
@:file("assets/preloader/flash/fml_preloader.swf") class __ASSET__assets_preloader_flash_fml_preloader_swf extends lime.utils.ByteArray {}
@:bitmap("assets/preloader/html5/fairy_preloader_bg.png") class __ASSET__assets_preloader_html5_fairy_preloader_bg_png extends lime.graphics.Image {}
@:bitmap("assets/preloader/html5/id_net_logo.png") class __ASSET__assets_preloader_html5_id_net_logo_png extends lime.graphics.Image {}
@:bitmap("assets/preloader/html5/Y8_logo.png") class __ASSET__assets_preloader_html5_y8_logo_png extends lime.graphics.Image {}
@:file("assets/screen1_f.json") class __ASSET__assets_screen1_f_json extends lime.utils.ByteArray {}
@:file("assets/screen1_f_bck.json") class __ASSET__assets_screen1_f_bck_json extends lime.utils.ByteArray {}
@:file("assets/screen2_f.json") class __ASSET__assets_screen2_f_json extends lime.utils.ByteArray {}
@:file("assets/screen3_f.json") class __ASSET__assets_screen3_f_json extends lime.utils.ByteArray {}
@:file("assets/screens/doll_f.json") class __ASSET__assets_screens_doll_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/doll_f.png") class __ASSET__assets_screens_doll_f_png extends lime.graphics.Image {}
@:file("assets/screens/doll_small_f.json") class __ASSET__assets_screens_doll_small_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/doll_small_f.png") class __ASSET__assets_screens_doll_small_f_png extends lime.graphics.Image {}
@:file("assets/screens/finalScreen_f.json") class __ASSET__assets_screens_finalscreen_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/finalScreen_f.png") class __ASSET__assets_screens_finalscreen_f_png extends lime.graphics.Image {}
@:file("assets/screens/menu_f.json") class __ASSET__assets_screens_menu_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/menu_f.png") class __ASSET__assets_screens_menu_f_png extends lime.graphics.Image {}
@:file("assets/screens/pauseMenu_f.json") class __ASSET__assets_screens_pausemenu_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/pauseMenu_f.png") class __ASSET__assets_screens_pausemenu_f_png extends lime.graphics.Image {}
@:file("assets/screens/screen1_f.json") class __ASSET__assets_screens_screen1_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/screen1_f.png") class __ASSET__assets_screens_screen1_f_png extends lime.graphics.Image {}
@:file("assets/screens/screen2_f.json") class __ASSET__assets_screens_screen2_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/screen2_f.png") class __ASSET__assets_screens_screen2_f_png extends lime.graphics.Image {}
@:file("assets/screens/screen3_f.json") class __ASSET__assets_screens_screen3_f_json extends lime.utils.ByteArray {}
@:bitmap("assets/screens/screen3_f.png") class __ASSET__assets_screens_screen3_f_png extends lime.graphics.Image {}
@:file("assets/sliders/accessory.json") class __ASSET__assets_sliders_accessory_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/accessory.png") class __ASSET__assets_sliders_accessory_png extends lime.graphics.Image {}
@:file("assets/sliders/brows.json") class __ASSET__assets_sliders_brows_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/brows.png") class __ASSET__assets_sliders_brows_png extends lime.graphics.Image {}
@:file("assets/sliders/cloth_bottom.json") class __ASSET__assets_sliders_cloth_bottom_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/cloth_bottom.png") class __ASSET__assets_sliders_cloth_bottom_png extends lime.graphics.Image {}
@:file("assets/sliders/cloth_top.json") class __ASSET__assets_sliders_cloth_top_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/cloth_top.png") class __ASSET__assets_sliders_cloth_top_png extends lime.graphics.Image {}
@:file("assets/sliders/eyes.json") class __ASSET__assets_sliders_eyes_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/eyes.png") class __ASSET__assets_sliders_eyes_png extends lime.graphics.Image {}
@:file("assets/sliders/hairs.json") class __ASSET__assets_sliders_hairs_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/hairs.png") class __ASSET__assets_sliders_hairs_png extends lime.graphics.Image {}
@:file("assets/sliders/ink.json") class __ASSET__assets_sliders_ink_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/ink.png") class __ASSET__assets_sliders_ink_png extends lime.graphics.Image {}
@:file("assets/sliders/jewelry.json") class __ASSET__assets_sliders_jewelry_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/jewelry.png") class __ASSET__assets_sliders_jewelry_png extends lime.graphics.Image {}
@:file("assets/sliders/pomade.json") class __ASSET__assets_sliders_pomade_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/pomade.png") class __ASSET__assets_sliders_pomade_png extends lime.graphics.Image {}
@:file("assets/sliders/rouge.json") class __ASSET__assets_sliders_rouge_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/rouge.png") class __ASSET__assets_sliders_rouge_png extends lime.graphics.Image {}
@:file("assets/sliders/shadows.json") class __ASSET__assets_sliders_shadows_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/shadows.png") class __ASSET__assets_sliders_shadows_png extends lime.graphics.Image {}
@:file("assets/sliders/wings.json") class __ASSET__assets_sliders_wings_json extends lime.utils.ByteArray {}
@:bitmap("assets/sliders/wings.png") class __ASSET__assets_sliders_wings_png extends lime.graphics.Image {}
@:sound("assets/music/music_fairy.mp3") class __ASSET__assets_music_music_fairy_mp4 extends lime.audio.AudioSource {}
@:sound("assets/music/button_click_1.mp3") class __ASSET__assets_music_button_click_2 extends lime.audio.AudioSource {}



#end

#end
#end

