import lime.Assets;
#if !macro


class ApplicationMain {
	
	
	public static var config:lime.app.Config;
	public static var preloader:openfl.display.Preloader;
	
	
	public static function create ():Void {
		
		var app = new openfl.display.Application ();
		app.create (config);
		
		var display = new TzPreloader ();
		
		preloader = new openfl.display.Preloader (display);
		preloader.onComplete = init;
		preloader.create (config);
		
		#if (js && html5)
		var urls = [];
		var types = [];
		
		
		urls.push ("assets/clock.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/doll_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/doll_small_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/finalScreen_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/loader/loader_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/loader/loader_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/masks/mask_f_1.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/masks/mask_f_1.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/masks/mask_f_2.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/masks/mask_f_2.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/masks/mask_f_3.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/masks/mask_f_3.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/menu_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/music/button_click_1.mp3");
		types.push (AssetType.MUSIC);
		
		
		urls.push ("assets/music/music_fairy.mp3");
		types.push (AssetType.MUSIC);
		
		
		urls.push ("assets/openfl.svg");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/pauseMenu_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/preloader/flash/fml_preloader.swf");
		types.push (AssetType.BINARY);
		
		
		urls.push ("assets/preloader/html5/fairy_preloader_bg.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/preloader/html5/id_net_logo.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/preloader/html5/Y8_logo.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screen1_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screen1_f_bck.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screen2_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screen3_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/doll_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/doll_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/doll_small_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/doll_small_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/finalScreen_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/finalScreen_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/menu_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/menu_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/pauseMenu_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/pauseMenu_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/screen1_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/screen1_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/screen2_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/screen2_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/screens/screen3_f.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/screens/screen3_f.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/accessory.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/accessory.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/brows.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/brows.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/cloth_bottom.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/cloth_bottom.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/cloth_top.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/cloth_top.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/eyes.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/eyes.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/hairs.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/hairs.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/ink.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/ink.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/jewelry.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/jewelry.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/pomade.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/pomade.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/rouge.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/rouge.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/shadows.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/shadows.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/sliders/wings.json");
		types.push (AssetType.TEXT);
		
		
		urls.push ("assets/sliders/wings.png");
		types.push (AssetType.IMAGE);
		
		
		urls.push ("assets/music/music_fairy.mp3");
		types.push (AssetType.SOUND);
		
		
		urls.push ("assets/music/button_click_1.mp3");
		types.push (AssetType.SOUND);
		
		
		
		if (config.assetsPrefix != null) {
			
			for (i in 0...urls.length) {
				
				if (types[i] != AssetType.FONT) {
					
					urls[i] = config.assetsPrefix + urls[i];
					
				}
				
			}
			
		}
		
		preloader.load (urls, types);
		#end
		
		var result = app.exec ();
		
		#if (sys && !emscripten)
		Sys.exit (result);
		#end
		
	}
	
	
	public static function init ():Void {
		
		var loaded = 0;
		var total = 0;
		var library_onLoad = function (__) {
			
			loaded++;
			
			if (loaded == total) {
				
				start ();
				
			}
			
		}
		
		preloader = null;
		
		
		
		if (loaded == total) {
			
			start ();
			
		}
		
	}
	
	
	public static function main () {
		
		config = {
			
			antialiasing: Std.int (0),
			background: Std.int (0),
			borderless: false,
			depthBuffer: false,
			fps: Std.int (30),
			fullscreen: false,
			height: Std.int (533),
			orientation: "",
			resizable: true,
			stencilBuffer: false,
			title: "Fairy MakeUp Lily",
			vsync: false,
			width: Std.int (800),
			
		}
		
		#if js
		#if (munit || utest)
		flash.Lib.embed (null, 800, 533, "000000");
		#end
		#else
		create ();
		#end
		
	}
	
	
	public static function start ():Void {
		
		var hasMain = false;
		var entryPoint = Type.resolveClass ("Main");
		
		for (methodName in Type.getClassFields (entryPoint)) {
			
			if (methodName == "main") {
				
				hasMain = true;
				break;
				
			}
			
		}
		
		if (hasMain) {
			
			Reflect.callMethod (entryPoint, Reflect.field (entryPoint, "main"), []);
			
		} else {
			
			var instance:DocumentClass = Type.createInstance (DocumentClass, []);
			
			/*if (Std.is (instance, openfl.display.DisplayObject)) {
				
				openfl.Lib.current.addChild (cast instance);
				
			}*/
			
		}
		
		openfl.Lib.current.stage.dispatchEvent (new openfl.events.Event (openfl.events.Event.RESIZE, false, false));
		
	}
	
	
	#if neko
	@:noCompletion public static function __init__ () {
		
		var loader = new neko.vm.Loader (untyped $loader);
		loader.addPath (haxe.io.Path.directory (Sys.executablePath ()));
		loader.addPath ("./");
		loader.addPath ("@executable_path/");
		
	}
	#end
	
	
}


@:build(DocumentClass.build())
@:keep class DocumentClass extends Main {}


#else


import haxe.macro.Context;
import haxe.macro.Expr;


class DocumentClass {
	
	
	macro public static function build ():Array<Field> {
		
		var classType = Context.getLocalClass ().get ();
		var searchTypes = classType;
		
		while (searchTypes.superClass != null) {
			
			if (searchTypes.pack.length == 2 && searchTypes.pack[1] == "display" && searchTypes.name == "DisplayObject") {
				
				var fields = Context.getBuildFields ();
				
				var method = macro {
					
					openfl.Lib.current.addChild (this);
					super ();
					dispatchEvent (new openfl.events.Event (openfl.events.Event.ADDED_TO_STAGE, false, false));
					
				}
				
				fields.push ({ name: "new", access: [ APublic ], kind: FFun({ args: [], expr: method, params: [], ret: macro :Void }), pos: Context.currentPos () });
				
				return fields;
				
			}
			
			searchTypes = searchTypes.superClass.t.get ();
			
		}
		
		return null;
		
	}
	
	
}


#end
